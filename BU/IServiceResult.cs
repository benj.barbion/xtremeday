﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BU
{
    public interface IServiceResult 
    {
        ServiceResultStatus Status { get; set; }
        object ObjectResult { get; set; }
        string Message { get; set; }
    }
}
