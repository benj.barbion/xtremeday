﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BU
{
    public class ServiceBase 
    {
        /// <summary>
        /// Renvoie un ServiceResult<T> Ok avec un typage spécifié
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result"></param>
        /// <returns></returns>
        public ServiceResult<T> Ok<T>(T result)
        {
            return new ServiceResult<T>()
            {
                Status = ServiceResultStatus.Ok,
                ObjectResult = result
            };
        }

        public IServiceResult Ok(object objectResult = null, string message = null) => this.CreateServiceResult(ServiceResultStatus.Ok, objectResult, message);
        public ServiceResult Ko(string message = "Malformation dans la demande.") => this.CreateServiceResult(ServiceResultStatus.Ko, null, message);
        public ServiceResult BadRequest(string message = "Malformation dans la demande.") => this.CreateServiceResult(ServiceResultStatus.BadRequest, null, message);
        public ServiceResult AlreadyExist(string message = "L'information existe déjà.") => this.CreateServiceResult(ServiceResultStatus.AlreadyExist, null, message);
        public ServiceResult NotFound(string message = "Information non trouvée.") => this.CreateServiceResult(ServiceResultStatus.NotFound, null, message);

        private ServiceResult CreateServiceResult(ServiceResultStatus status, object objectResult = null, string message = null)
        {
            return new ServiceResult()
            {
                Status = status,
                ObjectResult = objectResult,
                Message = message
            };
        }
    }
}
