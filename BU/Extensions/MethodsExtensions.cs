﻿using BU.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace BU.Extensions
{
    /// <summary>
    /// Méthodes d'extensions
    /// </summary>
    public static class MethodsExtensions
    {
        /// <summary>
        /// Vérifie si un strinc contient un autre string, tout en omettant la case
        /// </summary>
        /// <param name="this">String sur lequel agir</param>
        /// <param name="value">Valeur à rechercher</param>
        /// <returns>True si le string contient l'autre, sinon False</returns>
        public static bool ContainsIgnoreCase(this string @this, string value)
        {
            return @this.IndexOf(value, StringComparison.CurrentCultureIgnoreCase) != -1;
        }

        /// <summary>
        /// Récupère l'âge selon une date passée
        /// </summary>
        /// <param name="this">La date sur laquelle agir</param>
        /// <returns>Un entier de l'âge</returns>
        public static int Age(this DateTime @this)
        {
            if (DateTime.Today.Month < @this.Month ||
                DateTime.Today.Month == @this.Month &&
                DateTime.Today.Day < @this.Day)
            {
                return DateTime.Today.Year - @this.Year - 1;
            }
            return DateTime.Today.Year - @this.Year;
        }

        /// <summary>
        /// Vérifie si une adresse email est correcte
        /// </summary>
        /// <param name="this">String sur lequel agir</param>
        /// <returns>True si email correct, sinon False</returns>
        public static bool IsValidEmail(this string @this)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(@this);
                return addr.Address == @this;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Vérifie si un jour est un jour férié
        /// </summary>
        /// <param name="this">Datetime sur lequel agir</param>
        /// <returns>True si jour férié, sinon False</returns>
        public static bool IsClosedDay(this DateTime @this)
        {
            return CalendarHelper.IsClosedDay(@this);
        }

        /// <summary>
        /// Vérifie si une date est un jour de semaine
        /// </summary>
        /// <param name="this">Datetime sur lequel agir</param>
        /// <returns>True si c'est un jour de la semaine, sinon False</returns>
        public static bool IsWeekDay(this DateTime @this)
        {
            return !(@this.DayOfWeek == DayOfWeek.Saturday || @this.DayOfWeek == DayOfWeek.Sunday);
        }

        /// <summary>
        /// Permet de récupérer la description d'une énumération
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumValue"></param>
        /// <returns>string</returns>
        public static string GetDescription<T>(this T enumValue) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
                return null;

            var description = enumValue.ToString();
            var fieldInfo = enumValue.GetType().GetField(enumValue.ToString());

            if (fieldInfo != null)
            {
                var attrs = fieldInfo.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    description = ((DescriptionAttribute)attrs[0]).Description;
            }

            return description;
        }

        /// <summary>
        /// Permet d'effectuer une action sur une collection de type IEnumerable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumerable">Collection sur laquelle agir</param>
        /// <param name="action">Action à effectuer</param>
        public static void Action<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (T element in enumerable)
                action(element);
        }
    }
}
