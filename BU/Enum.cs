﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BU
{
    public enum TraineeshipLevel
    {
        FACILE = 1,
        MOYEN = 2,
        DIFFICILE = 3
    }  

    public enum TraineeshipStatus
    {
        OUVERT = 1,
        ANNULE = 2,
        TERMINE = 3,
    }

    public enum RegistrationStatus
    {
        [Description("Inscription en attente de paiement.")]
        OUVERT = 1,

        [Description("Inscription validée et payée.")]
        VALIDE = 2,

        ANNULE = 3,
    }

    public enum Perms
    {
        [Description("Permet de modifier les permissions des rôles.")]
        canManagePerm = 1,

        [Description("Permet de gérer les utilisateurs.")]
        canManageUser = 2,

        [Description("Permet de gérer la liste des sports.")]
        canManageSport = 3,

        [Description("Permet de gérer la liste des services.")]
        canManageService = 4,

        [Description("Permet de gérer la liste des localités.")]
        canManageEntity = 5
    }
}
