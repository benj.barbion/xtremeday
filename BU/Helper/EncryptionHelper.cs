﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace BU.Helper
{
    public class EncryptionHelper
    {
        /// <summary>
        /// Méthode permettant d'encrypter un mot de passe
        /// </summary>
        /// <param name="strData">string à encoder</param>
        /// <returns>string encodé</returns>
        public static string Encrypt(string pass)
        {
            return Convert.ToBase64String(Encrypt(Encoding.UTF8.GetBytes(pass)));
        }

        /// <summary>
        /// Méthode permettant d'encrypter un mot de passe selon une taille et de renvoyer également le mot de passe en clair
        /// </summary>
        /// <param name="length">Longueur du mot de passe</param>
        /// <param name="password">Variable de sortie mot de passe non crypté</param>
        /// <returns>string encodé</returns>
        public static string Encrypt(int length, out string password)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            password = new string(Enumerable.Repeat(chars, length).Select(s => s[new Random().Next(s.Length)]).ToArray());
            return Encrypt(password);
        }

        private static byte[] Encrypt(byte[] strData)
        {
            PasswordDeriveBytes passbytes = new("XtremeDay", new byte[] { 0x19, 0x59, 0x17, 0x41 });
            MemoryStream memstream = new();
            Aes aes = new AesManaged();
            aes.Key = passbytes.GetBytes(aes.KeySize / 8);
            aes.IV = passbytes.GetBytes(aes.BlockSize / 8);
            CryptoStream cryptostream = new(memstream, aes.CreateEncryptor(), CryptoStreamMode.Write);
            cryptostream.Write(strData, 0, strData.Length);
            cryptostream.Close();
            return memstream.ToArray();
        }
    }
}
