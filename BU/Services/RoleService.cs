﻿using AutoMapper;
using BU.Models;
using DAL;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace BU.Services
{
    public class RoleService : GenericService<Role, RoleModel>, IRoleService
    {
        public RoleService(IMapper mapper, IDbContextFactory<XtremeDayContext> dbContextFactory) : base(mapper, dbContextFactory)
        {
        }

        public ServiceResult<int> RemovePerm(RoleModel role, string perm)
        {
            if (role is null || perm is null)
                return BadRequest("Malformation dans votre requête.");

            using var context = this.DbContextFactory.CreateDbContext();
            
            var dbRole = context.Roles.Include(x => x.RolePerms).ThenInclude(x => x.IdPermNavigation).FirstOrDefault(x => x.Id == role.Id);
            if (dbRole is null)
                return NotFound("Le rôle n'existe pas.");

            var rolePerms = dbRole.RolePerms.FirstOrDefault(rp => rp.IdPermNavigation.Name == perm);
            if (rolePerms is null)
                return NotFound("La permission n'existe pas dans ce rôle.");

            dbRole.RolePerms.Remove(rolePerms);
            return Ok(context.SaveChanges());
        }

        public ServiceResult<ICollection<PermModel>> GetPerms(RoleModel role)
        {
            if (role is null)
                return BadRequest("Le role ne peut pas être null");
            
            using var context = this.DbContextFactory.CreateDbContext();
            
            var allPerms = context.RolePerms
                .Include(rp => rp.IdPermNavigation)
                .Where(rp => rp.IdRole == role.Id)
                .Select(p => p.IdPermNavigation);

            return Ok(mapper.Map<ICollection<PermModel>>(allPerms));
        }

        public ServiceResult<bool> RoleExist(string role)
        {
            if (role is null)
                return BadRequest("Le role ne peut pas être null");

            using var context = this.DbContextFactory.CreateDbContext();
            
            return Ok(context.Roles.Any(x => x.Name == role));
        }

        public ServiceResult<PermModel> AddPerm(RoleModel role, string perm)
        {
            if (role is null || perm is null)
                return BadRequest("Malformation dans votre requête.");

            using var context = this.DbContextFactory.CreateDbContext();

            var dbRole = context.Roles.Include(x => x.RolePerms).FirstOrDefault(x => x.Id == role.Id);
            if (dbRole is null)
                return NotFound("Le rôle n'existe pas.");

            var dbPerm = context.Perms.FirstOrDefault(x => x.Name == perm);
            if (dbPerm is null)
                return NotFound("La permission n'existe pas.");

            if (dbRole.RolePerms.Any(x => x.IdPerm == dbPerm.Id))
                return AlreadyExist("Le rôle possède déjà cette permission.");

            var rolePerm = new RolePerm() { IdPerm = dbPerm.Id };

            dbRole.RolePerms.Add(rolePerm);

            try
            {
                context.SaveChanges();
            }
            catch (global::System.Exception)
            {
                return Ko();
            }

            return Ok(mapper.Map<PermModel>(dbPerm));
        }
    }
}
