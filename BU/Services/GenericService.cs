﻿using AutoMapper;
using DAL;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BU.Services
{
    public class GenericService<TEntity, TModel> : ServiceBase, IGenericService<TModel> where TEntity : class where TModel : class
    {
        protected readonly IMapper mapper;

        public GenericService(IMapper mapper, IDbContextFactory<XtremeDayContext> dbContextFactory)
        {
            this.mapper = mapper;
            this.DbContextFactory = dbContextFactory;
        }

        public int Count()
        {
            using var db = this.DbContextFactory.CreateDbContext();
            return db.Set<TEntity>().Count();
        }

        public virtual ICollection<TModel> GetAll()
        {
            using var db = this.DbContextFactory.CreateDbContext();
            return this.mapper.Map<ICollection<TModel>>(db.Set<TEntity>());
        }

        public virtual ServiceResult<TModel> Create(TModel model)
        {
            try
            {
                using var db = this.DbContextFactory.CreateDbContext();
                var entity = this.ConvertToEntity(model);
                db.Add<TEntity>(entity);
                db.SaveChanges();
                var result = this.ConvertToModel(entity);
                return Ok(result);
            }
            catch (Exception)
            {
                return Ko();
            }
        }

        public virtual ServiceResult<TModel> Update(TModel model)
        {
            try
            {
                using var db = this.DbContextFactory.CreateDbContext();
                var entity = this.ConvertToEntity(model);
                db.Set<TEntity>().Update(entity);
                db.SaveChanges();
                return Ok(this.ConvertToModel(entity));
            }
            catch (Exception)
            {
                return Ko();
            }           
        }

        public ServiceResult<int> Delete(int id)
        {
            try
            {
                using var db = this.DbContextFactory.CreateDbContext();
                var data = db.Set<TEntity>().Find(id);
                if (data is null)
                    return NotFound();           
                db.Set<TEntity>().Remove(data);              
                return Ok(db.SaveChanges());
            }
            catch (Exception)
            {
                return Ko();
            }
        }

        public ServiceResult<int> Delete(TModel model)
        {
            try
            {
                using var db = this.DbContextFactory.CreateDbContext();
                var entity = this.ConvertToEntity(model);
                db.Set<TEntity>().Remove(entity);
                return Ok(db.SaveChanges());
            }
            catch (Exception)
            {
                return Ko();
            }
        }

        public virtual ICollection<TModel> Find(Expression<Func<TModel, bool>> predicate)
        {
            try
            {
                using var db = this.DbContextFactory.CreateDbContext();
                var mappPredicate = this.mapper.Map<Expression<Func<TEntity, bool>>>(predicate);
                var data = db.Set<TEntity>().Where(mappPredicate).AsNoTracking();
                return mapper.Map<ICollection<TModel>>(data);
            }
            catch (Exception)
            {
                return null;
            }  
        }

        public virtual TModel FindById(int id)
        {
            using var db = this.DbContextFactory.CreateDbContext();
            var data = db.Set<TEntity>().Find(id);
            return data is not null ? this.ConvertToModel(data) : null;
        }

        public virtual ICollection<TModel> Take(int total = 20, int skip = 0)
        {
            using var db = this.DbContextFactory.CreateDbContext();
            return mapper.Map<ICollection<TModel>>(db.Set<TEntity>().Take(total).Skip(skip));
        }

        /// <summary>
        /// Converti automatiquement un modèle en entité
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        protected TEntity ConvertToEntity(TModel model) => mapper.Map<TEntity>(model);

        /// <summary>
        /// Converti automatiquement une entité en modèle
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected TModel ConvertToModel(TEntity entity) => mapper.Map<TModel>(entity);

        public IDbContextFactory<XtremeDayContext> DbContextFactory { get; private set; }
    }
}