﻿using AutoMapper;
using BU.Extensions;
using BU.Models;
using DAL;
using DAL.Models;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BU.Services
{
    public class TraineeshipService : GenericService<Traineeship, TraineeshipModel>, ITraineeshipService
    {
        public TraineeshipService(IMapper mapper, IDbContextFactory<XtremeDayContext> context) : base(mapper, context)
        {
        }

        public override ICollection<TraineeshipModel> Find(Expression<Func<TraineeshipModel, bool>> predicate)
        {
            using var context = this.DbContextFactory.CreateDbContext();
            
            var mappPredicate = this.mapper.Map<Expression<Func<Traineeship, bool>>>(predicate);
            var traineeships = context.Traineeships.Include(t => t.IdTraineeshipStatusNavigation).Where(mappPredicate);
            
            return mapper.Map<ICollection<TraineeshipModel>>(traineeships);
        }

        public override ServiceResult<TraineeshipModel> Create(TraineeshipModel model)
        {
            // We can check some business rules here 
            var result = this.ValidateModel(model);

            if (result.Status != ServiceResultStatus.Ok)
                return result;
            
            if (model.MaxInscription < 1)
                model.MaxInscription = 1;

            model.Status = TraineeshipStatus.OUVERT;
            model.RemainingInscription = model.MaxInscription;

            return base.Create(model);
        }

        public override ServiceResult<TraineeshipModel> Update(TraineeshipModel model)
        {
            var result = this.ValidateModel(model);
            if (result.Status != ServiceResultStatus.Ok)
                return result;

            using var context = this.DbContextFactory.CreateDbContext();

            var trainee = context.Traineeships
                .Include(t => t.TraineeshipServices)
                .Include(t => t.Registrations)
                    .ThenInclude(t => t.RegistrationClients)
                .FirstOrDefault(x => x.Id == model.Id);            
            
            if (trainee is null)
                return NotFound();

            if (trainee.IdTraineeshipStatus != (int)TraineeshipStatus.OUVERT)
                 return BadRequest($"Vous ne pouvez pas modifier un stage non ouvert.");

            var totalClients = trainee.Registrations.Where(r => r.IdRegistrationStatus != (int)RegistrationStatus.ANNULE).Sum(r => r.RegistrationClients.Count);
            if (model.MaxInscription < totalClients)
                return BadRequest($"Trop peu de places pour le nombre de personnes déjà inscrites : {totalClients}.");

            var difference = model.MaxInscription - trainee.MaxInscription;
            model.RemainingInscription += (short)difference;
            trainee.TraineeshipServices.ForEach(x => context.TraineeshipServices.Remove(x));

            try
            {
                context.SaveChanges();
                return base.Update(model);
            }
            catch (Exception)
            {
                return Ko();
            }
        }

        public int Count(TraineeSearchFilterModel filter)
        {
            using var context = this.DbContextFactory.CreateDbContext();
            return context.Traineeships.Where(FilterToExpression(filter)).Count();
        }

        public override ICollection<TraineeshipModel> Take(int id = 20, int skip = 0) => this.Take(id, skip, null);

        public ICollection<TraineeshipModel> Take(int total = 20, int skip = 0, TraineeSearchFilterModel filter = null)
        {
            using var context = this.DbContextFactory.CreateDbContext();
            var traineeships = context.Traineeships
                                .AsNoTracking()
                                .Include(t => t.IdLevelNavigation)
                                .Include(t => t.IdSportNavigation)
                                .Include(t => t.IdTraineeshipStatusNavigation)
                                .Include(t => t.IdEntityNavigation)
                                .Include("TraineeshipServices.IdServiceNavigation");

            if (filter is not null)
                traineeships = traineeships.Where(FilterToExpression(filter));

            traineeships = traineeships.OrderByDescending(x => x.Id).Skip(skip).Take(total);

            return mapper.Map<ICollection<TraineeshipModel>>(traineeships);
        }

        /// <summary>
        /// Produit un prédicat selon le filtre passé
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        private Expression<Func<Traineeship, bool>> FilterToExpression(TraineeSearchFilterModel filter)
        {
            var predicate = PredicateBuilder.New<Traineeship>(true);

            if (filter.Code is not null)
                predicate.And(t => t.Code == filter.Code);

            if (filter.PriceFrom is not null)
                predicate.And(t => t.PricePerPerson >= filter.PriceFrom);

            if (filter.PriceTo is not null)
                predicate.And(t => t.PricePerPerson <= filter.PriceTo);

            if (filter.Status is not null)
                predicate.And(t => t.IdTraineeshipStatus == filter.Status.Id);

            if (filter.DateFrom is not null)
                predicate.And(t => t.StartDate >= filter.DateFrom);

            if (filter.DateTo is not null)
                predicate.And(t => t.EndDate <= filter.DateTo);

            if (filter.RemainingInscription is not null)
                predicate.And(t => t.RemainingInscription >= filter.RemainingInscription);

            if (filter.Levels?.Count > 0)
                predicate.And(t => filter.Levels.Select(l => l.Id).Contains(t.IdLevel));

            if (filter.Sports?.Count > 0)
                predicate.And(t => filter.Sports.Select(s => s.Id).Contains(t.IdSport));

            if (filter.Entities?.Count > 0)
                predicate.And(t => filter.Entities.Select(e => e.Id).Contains(t.IdEntity));

            if (filter.Services?.Count > 0)
                predicate.And(t => t.TraineeshipServices.Any(x => filter.Services.Select(s => s.Id).Contains(x.IdService)));

            return predicate;
        }

        /// <summary>
        /// Vérifie le modèle passé
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private ServiceResult<TraineeshipModel> ValidateModel(TraineeshipModel model)
        {
            if (model is null)
                return BadRequest("Le modèle n'existe pas.");

            if (model.StartDate == default || (model.StartDate.IsWeekDay() && !model.StartDate.IsClosedDay()))
                return BadRequest("La date de départ doit être comprise dans un weekend ou un jour férié.");

            if (model.EndDate == default || (model.EndDate.IsWeekDay() && !model.EndDate.IsClosedDay()))
                return BadRequest("La date de fin doit être comprise dans un weekend ou un jour férié.");

            if (model.EndDate <= model.StartDate)
                return BadRequest("La date de fin ne peut pas être inférieure ou égale à la date de début.");

            if (model?.Title?.Length < 3)
                return BadRequest("Le titre doit contenir au minimum 3 caractères.");

            if (model.MaxInscription <= 0)
                return BadRequest("Il doit y avoir au minimum une place.");

            if (model.PricePerPerson <= 0)
                return BadRequest("Le prix ne peut pas être gratuit.");

            if (model.Address is null)
                return BadRequest("L'adresse n'existe pas.");

            if (model.Entity is null)
                return BadRequest("Ville non indiquée.");

            if (model.Level is null)
                return BadRequest("Niveau non indiqué.");

            if (model.Sport is null)
                return BadRequest("Sport non indiqué.");

            return Ok(model);
        }
    }
}
