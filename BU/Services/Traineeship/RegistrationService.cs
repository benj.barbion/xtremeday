﻿using AutoMapper;
using BU.Models;
using DAL;
using DAL.Models;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace BU.Services
{
    public class RegistrationService : GenericService<Registration, RegistrationModel>, IRegistrationService
    {
        public RegistrationService(IMapper mapper, IDbContextFactory<XtremeDayContext> dbContextFactory) : base(mapper, dbContextFactory)
        {
        }

        public IServiceResult ValidateRegistration(int id)
        {
            using var context = this.DbContextFactory.CreateDbContext();

            var registration = context.Registrations.Include(r => r.RegistrationClients).FirstOrDefault(r => r.Id == id);
            if (registration == null)
                return NotFound();

            if (registration.IdRegistrationStatus != (int)RegistrationStatus.OUVERT)
                return BadRequest("Cette inscription ne possède pas le statut ouvert et ne peut donc pas être validée.");

            registration.IdRegistrationStatus = (int)RegistrationStatus.VALIDE;

            try
            {
                context.SaveChanges();
                return Ok();
            }
            catch (Exception)
            {
                return Ko();
            }
        }

        public IServiceResult CancelRegistration(int id)
        {
            using var context = this.DbContextFactory.CreateDbContext();

            var registration = context.Registrations.Include(r => r.RegistrationClients).FirstOrDefault(r => r.Id == id);

            if (registration == null)
                return NotFound();

            if (registration.IdRegistrationStatus == (int)RegistrationStatus.ANNULE)
                return BadRequest("Cette inscription est déjà annulée.");

            registration.IdRegistrationStatus = (int)RegistrationStatus.ANNULE;

            // if it's canceled, we have to restore back remaining place !
            var trainee = context.Traineeships.Find(registration.IdTraineeship);
            if (trainee is null)
                return Ko();

            trainee.RemainingInscription += (short)registration.RegistrationClients.Count;

            try
            {
                context.SaveChanges();
                return Ok();
            }
            catch (Exception)
            {
                return Ko();
            }
        }

        public override ServiceResult<RegistrationModel> Create(RegistrationModel model)
        {
            if (model is null)
                return BadRequest("Malformation dans la demande");

            model.Status = new StatusModel()
            {
                Id = (int)RegistrationStatus.OUVERT // par défaut, le statut d'ine inscription est OUVERT
            };

            using var context = this.DbContextFactory.CreateDbContext();
            
            var trainee = context.Traineeships.Find(model.Traineeship.Id);
            if (trainee is null)
                return NotFound();

            if (trainee.RemainingInscription < model.RegistrationClients.Count)
                return Ko("Le nombre de places demandées n'est plus disponible.");

            if (DateTime.Now >= trainee.StartDate.AddDays(-1))
                return BadRequest("Plus d'inscription possible à ce stage (délais expirés).");

            context.Add(this.ConvertToEntity(model));
            
            trainee.RemainingInscription = (short)(trainee.RemainingInscription - model.RegistrationClients.Count);

            try
            {
                context.SaveChanges();
                return Ok(model);
            }
            catch (Exception)
            {
                return Ko();
            }
        }

        public override ICollection<RegistrationModel> Take(int total = 20, int skip = 0) => this.Take(total, skip, null);

        public ICollection<RegistrationModel> Take(int total = 20, int skip = 0, RegistrationSearchFilterModel filter = null)
        {
            using var context = this.DbContextFactory.CreateDbContext();

            var datas = context.Registrations
                .Include(x => x.RegistrationClients)
                    .ThenInclude(x => x.IdClientNavigation)
                        .ThenInclude(x => x.IdPersonNavigation)
                .Include(x => x.IdResponsibleClientNavigation)
                    .ThenInclude(x => x.IdPersonNavigation)
                .Include(x => x.IdTraineeshipNavigation)
                .Include(x => x.IdRegistrationStatusNavigation)
                .Where(FilterToExpression(filter))
                .Skip(skip)
                .Take(total)
                .OrderByDescending(x => x.Id);

            return this.mapper.Map<ICollection<RegistrationModel>>(datas);
        }

        public int Count(RegistrationSearchFilterModel filter)
        {
            var predicate = this.mapper.Map<Expression<Func<Registration, bool>>>(FilterToExpression(filter));
            using var context = this.DbContextFactory.CreateDbContext();
            return context.Registrations.Where(predicate).Count();
        }

        /// <summary>
        /// Produit un prédicat selon le filtre passé
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        private Expression<Func<Registration, bool>> FilterToExpression(RegistrationSearchFilterModel filter)
        {
            var predicate = PredicateBuilder.New<Registration>(true);
            
            if (filter is null)
                return predicate;

            if (filter.CodeRegistration is not null)
                predicate.And(r => r.Code == filter.CodeRegistration);

            if (filter.CodeTrainee is not null)
                predicate.And(r => r.IdTraineeshipNavigation.Code == filter.CodeTrainee);

            if (filter.EmailResponsable is not null)
                predicate.And(r => r.IdResponsibleClientNavigation.IdPersonNavigation.Email == filter.EmailResponsable);

            if (filter.RegistrationStatus is not null)
                predicate.And(r => r.IdRegistrationStatus == filter.RegistrationStatus.Id);

            if (filter.From is not null)
                predicate.And(r => r.Date >= filter.From);

            if (filter.To is not null)
                predicate.And(r => r.Date <= filter.To);

            return predicate;
        }
    }
}
