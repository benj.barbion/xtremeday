﻿using BU.Models;
using System.Collections.Generic;

namespace BU.Services
{
    public interface IRegistrationService : IGenericService<RegistrationModel>
    {
        /// <summary>
        /// Permet de récupérer un ensemble de données selon un total, un possible saut et possible un filtre
        /// </summary>
        /// <param name="total">total de donnée à récupérer</param>
        /// <param name="skip">saut à effectuer</param>
        /// <param name="filter">filtre à appliquer</param>
        /// <returns></returns>
        ICollection<RegistrationModel> Take(int total = 20, int skip = 0, RegistrationSearchFilterModel filter = null);
        
        /// <summary>
        /// Compte le nombre d'entités respectant le filtre indiqué
        /// </summary>
        /// <param name="filter">Filtre de recherche</param>
        /// <returns></returns>
        int Count(RegistrationSearchFilterModel filter);

        /// <summary>
        /// Annuler une inscription
        /// </summary>
        /// <param name="id">ID de l'inscription</param>
        /// <returns></returns>
        IServiceResult CancelRegistration(int id);

        /// <summary>
        /// Valider une inscription
        /// </summary>
        /// <param name="id">Id de l'inscription</param>
        /// <returns></returns>
        IServiceResult ValidateRegistration(int id);
    }
}
