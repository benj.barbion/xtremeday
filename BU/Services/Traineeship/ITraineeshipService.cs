﻿using BU.Models;
using System.Collections.Generic;

namespace BU.Services
{
    public interface ITraineeshipService : IGenericService<TraineeshipModel>
    {
        /// <summary>
        /// Permet de récupérer un ensemble de données selon un total, un possible saut et possible un filtre
        /// </summary>
        /// <param name="total">total de donnée à récupérer</param>
        /// <param name="skip">saut à effectuer</param>
        /// <param name="filter">filtre à appliquer</param>
        /// <returns></returns>
        ICollection<TraineeshipModel> Take(int total = 20, int skip = 0, TraineeSearchFilterModel filter = null);
        
        /// <summary>
        /// Compte le nombre d'entités respectant le filtre indiqué
        /// </summary>
        /// <param name="filter">Filtre de recherche</param>
        /// <returns></returns>
        int Count(TraineeSearchFilterModel filter);
    }
}