﻿using AutoMapper;
using BU.Extensions;
using BU.Models;
using DAL;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace BU.Services
{
    public class LocationService : GenericService<Entity, EntityModel>, ILocationService
    {
        public LocationService(IMapper mapper, IDbContextFactory<XtremeDayContext> dbContextFactory) : base(mapper, dbContextFactory)
        {
        }

        public override ServiceResult<EntityModel> Create(EntityModel model)
        {
            if (model?.Name is null)
                return BadRequest("Pas de nom d'entité.");

            if (model?.PostalCode == 0)
                return BadRequest("Mauvais code postal.");

            using var context = this.DbContextFactory.CreateDbContext();
            
            if (context.Entities.Any(e => e.Name == model.Name))
                return AlreadyExist();

            return base.Create(model);
        }

        public ICollection<EntityModel> GetAllFrom(ProvinceModel province)
        {
            using var context = this.DbContextFactory.CreateDbContext();

            var entities = context.Provinces
                                .Include(x => x.Entities)
                                .FirstOrDefault(x => x.Id == province.Id)
                                .Entities
                                .OrderBy(x => x.PostalCode);

            return mapper.Map<ICollection<EntityModel>>(entities);
        }

        public ICollection<ProvinceModel> GetAllProvinces()
        {
            using var context = this.DbContextFactory.CreateDbContext();
            var provinces = mapper.Map<ICollection<ProvinceModel>>(context.Provinces);
            provinces.Action(p => p.Entities = new List<EntityModel>());
            return provinces;
        }

        public override ICollection<EntityModel> GetAll()
        {
            using var context = this.DbContextFactory.CreateDbContext();
            var entities = context.Entities.OrderBy(x => x.PostalCode);
            return mapper.Map<ICollection<EntityModel>>(entities);
        }
    }
}
