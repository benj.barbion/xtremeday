﻿using BU.Models;

namespace BU.Services
{
    public interface ISportService : IGenericService<SportModel>
    {
        /// <summary>
        /// Créer un sport selon un nom passé
        /// </summary>
        /// <param name="role">Le nom du sport à ajouter</param>
        /// <returns>ServiceResult SportModel, contiendra le sport ou le message d'erreur </returns>
        ServiceResult<SportModel> Create(string name);
    }
}
