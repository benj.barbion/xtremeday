﻿using AutoMapper;
using BU.Models;
using DAL;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BU.Services
{
    public class FacilitiesService : GenericService<Service, ServiceModel>, IFacilitiesService
    {
        public FacilitiesService(IMapper mapper, IDbContextFactory<XtremeDayContext> dbContextFactory) : base(mapper, dbContextFactory)
        {
        }

        public ServiceResult<ServiceModel> Create(string name, string desc = null)
        {
            if (name is null)
                return BadRequest("Le nom ne peut pas être vide.");

            using var context = this.DbContextFactory.CreateDbContext();
            
            if (context.Services.Any(x => x.Name == name))
                return AlreadyExist();

            return base.Create(new ServiceModel() { Name = name, Desc = desc });
        }
    }
}
