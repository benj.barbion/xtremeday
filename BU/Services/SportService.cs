﻿using AutoMapper;
using BU.Models;
using DAL;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BU.Services
{
    public class SportService : GenericService<Sport, SportModel>, ISportService
    {
        public SportService(IMapper mapper, IDbContextFactory<XtremeDayContext> dbContextFactory) : base(mapper, dbContextFactory)
        {
        }

        public ServiceResult<SportModel> Create(string name)
        {
            if (name is null)
                return BadRequest("Le nom du sport doit être complété.");

            using var context = this.DbContextFactory.CreateDbContext();

            if (context?.Sports?.Any(x => x.Name == name) ?? false)
                return AlreadyExist();

            return base.Create(new SportModel() { Name = name });
        }

        public override ServiceResult<SportModel> Update(SportModel model)
        {
            if (model is null)
                return BadRequest("Le nom du sport doit être complété.");

            using var context = this.DbContextFactory.CreateDbContext();

            if (context.Sports.Any(s => s.Name == model.Name))
                return BadRequest("Ce nom de sport existe déjà.");

            return base.Update(model);
        }      
    }
}
