﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BU.Services
{
    /// <summary>
    /// Interface générique de services
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public interface IGenericService<TModel> where TModel : class
    {
        /// <summary>
        /// Permet de trouver une information selon un prédicat
        /// </summary>
        /// <param name="predicate">Prédicat de recherche</param>
        /// <returns></returns>
        ICollection<TModel> Find(Expression<Func<TModel, bool>> predicate);

        /// <summary>
        /// Retourne le total du type de modèle
        /// </summary>
        int Count();

        /// <summary>
        /// Récupère toutes les entityés d'un type de modèle
        /// </summary>
        /// <returns>Collection des modèles</returns>
        ICollection<TModel> GetAll();

        /// <summary>
        /// Ajoute un nouveau modèle 
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ServiceResult du modèle</returns>
        ServiceResult<TModel> Create(TModel model);

        /// <summary>
        /// Mets à jour un modèle en base de donnée
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ServiceResult du modèle</returns>
        ServiceResult<TModel> Update(TModel model);

        /// <summary>
        /// Supprime un modèle selon son ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ServiceResult<int> Delete(int id);

        /// <summary>
        /// Supprime un modèle selon so object
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ServiceResult<int> Delete(TModel model);

        /// <summary>
        /// Permet de retrouver une entité selon son id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TModel FindById(int id);

        /// <summary>
        /// Prends l'information selon un total et un saut 
        /// </summary>
        /// <param name="total"></param>
        /// <param name="skip"></param>
        /// <returns></returns>
        ICollection<TModel> Take(int total = 20, int skip = 0);
    }
}
