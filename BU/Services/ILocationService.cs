﻿using BU.Models;
using System;
using System.Collections.Generic;

namespace BU.Services
{
    public interface ILocationService : IGenericService<EntityModel>
    {
        /// <summary>
        /// Permet de retourner toutes les localités d'une province
        /// </summary>
        /// <param name="province">La province</param>
        /// <returns>Les localités de la province</returns>
        ICollection<EntityModel> GetAllFrom(ProvinceModel province);

        /// <summary>
        /// Récupère toutes les provinces
        /// </summary>
        /// <returns>Collection des provinces</returns>
        ICollection<ProvinceModel> GetAllProvinces();
    }
}