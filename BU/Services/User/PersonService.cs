﻿using BU.Extensions;
using BU.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BU.Services.User
{
    public class PersonService : ServiceBase, IPersonService
    {
        public ServiceResult<PersonModel> ValidateModel(PersonModel model)
        {
            if (model is null)
                return BadRequest();

            if (model.Birtdhday.Age() < 18)
                return BadRequest("Age minimal de 18 ans.");

            if (model.Address is null)
                return BadRequest("Aucune adresse insérée.");

            if (model.FirstName?.Length < 3)
                return BadRequest("Problème dans le prénom.");

            if (model.LastName?.Length < 3)
                return BadRequest("Problème dans le nom.");

            if (!model.Email?.IsValidEmail() ?? false)
                return BadRequest("Email incorrect.");

            if (model.PhoneNumber is null)
                return BadRequest("Pas de numéro de téléphone.");

            if (model.Entity is null)
                return BadRequest("Problème de localisation.");

            return Ok(model);
        }
    }
}
