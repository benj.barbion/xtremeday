﻿using AutoMapper;
using BU.Helper;
using BU.Models;
using DAL;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace BU.Services.User
{
    public class AuthenticationService : ServiceBase, IAuthenticationService
    {
        private readonly IMapper mapper;
        private readonly IDbContextFactory<XtremeDayContext> dbContextFactory;

        public AuthenticationService(IMapper mapper, IDbContextFactory<XtremeDayContext> dbContextFactory)
        {
            this.mapper = mapper;
            this.dbContextFactory = dbContextFactory;
        }

        public ServiceResult<UserModel> AuthenticateUser(string username, string password)
        {
            using var context = this.dbContextFactory.CreateDbContext();

            var employee = context.Users
                            .Include(e => e.IdPersonNavigation)
                                .ThenInclude(e => e.IdEntityNavigation)
                            .Include(e => e.UserRoles)
                                .ThenInclude(e => e.IdRoleNavigation)
                                    .ThenInclude(e => e.RolePerms)
                                        .ThenInclude(e => e.IdPermNavigation)
                            .FirstOrDefault(e => e.IdPersonNavigation.Email == username && e.Password == EncryptionHelper.Encrypt(password));

            return employee is null ? NotFound() : Ok(mapper.Map<UserModel>(employee));
        }
    }
}
