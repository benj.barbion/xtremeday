﻿using BU.Models;

namespace BU.Services.User
{
    public interface IAuthenticationService
    {
        /// <summary>
        /// Authentifie un utilisateur
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>Retourne un ServiceResult de l'utilisateur, utilisateur si aucune erreur, snon message</returns>
        ServiceResult<UserModel> AuthenticateUser(string username, string password);
    }
}