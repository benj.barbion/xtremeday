﻿using BU.Models;
using System;
using System.Collections.Generic;

namespace BU.Services.User
{
    public interface IUserRoleService
    {
        bool AddToRole(UserModel user, String role);
        bool AddToRoles(UserModel user, IEnumerable<String> roles);
        ICollection<RoleModel> GetRoles(UserModel user);
        bool IsInRole(UserModel user, String role);
        bool RemoveFromRole(UserModel user, String role);
    }
}
