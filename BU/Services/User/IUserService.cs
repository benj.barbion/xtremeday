﻿using BU.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BU.Services
{
    public interface IUserService : IGenericService<UserModel>
    {
        /// <summary>
        /// Permet de modifier le mot de passe d'un utilisateur
        /// </summary>
        /// <param name="user"></param>
        /// <param name="currentPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        ServiceResult<int> ChangePassword(UserModel user, string currentPassword, string newPassword);
        
        /// <summary>
        /// Permet de désactiver un utilisateur
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        IServiceResult DesactivateUser(int userId);
    }
}