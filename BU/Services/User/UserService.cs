﻿using AutoMapper;
using BU.Extensions;
using BU.Helper;
using BU.Models;
using BU.Services.User;
using DAL;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BU.Services
{
    public class UserService : GenericService<DAL.Models.User,UserModel>, IUserService
    {
        readonly IPersonService personService;

        public UserService(IMapper mapper, IDbContextFactory<XtremeDayContext> dbContextFactory, IPersonService personService) : base(mapper, dbContextFactory)
        {
            this.personService = personService;
        }

        public override ServiceResult<UserModel> Create(UserModel user)
        {
            var validateModel = this.personService.ValidateModel(user.Person);
            if (validateModel.Status != ServiceResultStatus.Ok)
                return BadRequest(validateModel.Message);

            if (user.UserRoles?.Count == 0)
                return BadRequest("Aucun rôle sélectionné.");

            using var context = this.DbContextFactory.CreateDbContext();
            using var transaction = context.Database.BeginTransaction();
            try
            {
                if (context.Users.Any(x => x.IdPersonNavigation.Email == user.Person.Email))
                    return AlreadyExist("Utilisateur déjà existant !");

                int userID;
                
                var personInDb = context.People.FirstOrDefault(p => p.Email == user.Person.Email);
                if (personInDb is null)
                {
                    var person = mapper.Map<Person>(user.Person);
                    context.People.Add(person);
                    context.SaveChanges();
                    userID = person.Id;
                }
                else
                    userID = personInDb.Id;
                
                context.Users.Add(new DAL.Models.User()
                {
                    Password = EncryptionHelper.Encrypt(10, out string password),
                    IdPerson = userID,
                    UserRoles = user.UserRoles.Select(x => new UserRole() { IdRole = x.Id }).ToList()
                });

                context.SaveChanges();
                transaction.Commit();
                user.Password = password;
                return Ok(user);
            }
            catch (Exception e)
            {
                transaction.Rollback();
                return Ko();
            }
        }

        public override ICollection<UserModel> Take(int total = 20, int skip = 0)
        {
            using var context = this.DbContextFactory.CreateDbContext();
            var users = context.Users
                .Include(u => u.UserRoles)
                    .ThenInclude(u => u.IdRoleNavigation)
                .Include(u => u.IdPersonNavigation)
                    .ThenInclude(u => u.IdEntityNavigation)
                .OrderByDescending(u => u.Id)
                .Skip(skip)
                .Take(total);
           
            return mapper.Map<ICollection<UserModel>>(users);
        }

        public ServiceResult<int> ChangePassword(UserModel user, string currentPassword, string newPassword)
        {
            if (user is null || currentPassword is null || newPassword is null || currentPassword == newPassword)
                return BadRequest("Informations incorrectes.");

            using var context = this.DbContextFactory.CreateDbContext();
            var userDB = context.Users.FirstOrDefault(u => u.Id == user.Id);
            if (userDB is null)
                return NotFound("Utilisateur inexistant.");

            if (userDB.Password != EncryptionHelper.Encrypt(currentPassword))
                return NotFound("Mot de passe incorrect.");

            userDB.Password = EncryptionHelper.Encrypt(newPassword);

            try
            {
                return Ok(context.SaveChanges());
            }
            catch (Exception)
            {
                return Ko();
            }
        }

        public IServiceResult DesactivateUser(int userId)
        {
            using var context = this.DbContextFactory.CreateDbContext();
            
            var user = context.Users.Find(userId);
            if (user is null)
                return NotFound();

            user.Active = false;
            context.SaveChanges();
            return Ok();
        }
    }
}
