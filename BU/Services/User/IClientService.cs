﻿using BU.Models;
using System.Collections.Generic;

namespace BU.Services
{
    public interface IClientService : IGenericService<ClientModel>
    {
    }
}
