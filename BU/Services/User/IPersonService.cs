﻿using BU.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BU.Services.User
{
    public interface IPersonService
    {
        /// <summary>
        /// Valide un modèle d'utilisateur
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ServiceResult<PersonModel> ValidateModel(PersonModel model);
    }
}
