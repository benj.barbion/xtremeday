﻿using AutoMapper;
using BU.Models;
using BU.Services.User;
using DAL;
using DAL.Models;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace BU.Services
{
    public class ClientService : GenericService<Client, ClientModel>, IClientService
    {
        readonly IPersonService personService;

        public ClientService(IMapper mapper, IDbContextFactory<XtremeDayContext> dbContextFactory, IPersonService personService) : base(mapper, dbContextFactory)
        {
            this.personService = personService;
        }

        public override ServiceResult<ClientModel> Create(ClientModel model)
        {
            var validateModel = this.personService.ValidateModel(model.Person);
            if (validateModel.Status != ServiceResultStatus.Ok)
                return BadRequest(validateModel.Message);

            using var context = this.DbContextFactory.CreateDbContext();

            if (context.Clients.Any(c => c.IdPersonNavigation.Email == model.Person.Email || c.IdPersonNavigation.PhoneNumber == model.Person.PhoneNumber))
                return AlreadyExist("Utilisateur déjà existant !");

            return base.Create(model);
        }

        public override ICollection<ClientModel> Take(int total = 20, int skip = 0)
        {
            using var context = this.DbContextFactory.CreateDbContext();

            var clients = context.Clients.OrderByDescending(c => c.Id).Skip(skip).Take(total).Include(c => c.IdPersonNavigation).ThenInclude(c => c.IdEntityNavigation);

            return mapper.Map<ICollection<ClientModel>>(clients);
        }

        public override ICollection<ClientModel> GetAll()
        {
            using var context = this.DbContextFactory.CreateDbContext();

            var clients = context.Clients.Include(c => c.IdPersonNavigation);

            return this.mapper.Map<ICollection<ClientModel>>(clients);
        }
    }
}
