﻿using BU.Models;

namespace BU.Services
{
    public interface IFacilitiesService : IGenericService<ServiceModel>
    {
        /// <summary>
        /// Créer une facilité (service) pour un stage
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        /// <returns></returns>
        ServiceResult<ServiceModel> Create(string name, string desc = null);
    }
}
