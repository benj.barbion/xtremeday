﻿using BU.Models;
using System.Collections.Generic;

namespace BU.Services
{
    public interface IRoleService : IGenericService<RoleModel>
    {
        /// <summary>
        /// Ajoute une permission dans un rôle
        /// </summary>
        /// <param name="role">Le rôle dans lequel ajouter la permission</param>
        /// <param name="perm">Nom de la permission</param>
        /// <returns>ServiceResult</returns>
        ServiceResult<PermModel> AddPerm(RoleModel role, string perm);

        /// <summary>
        /// Retourne la liste des permission d'un rôle
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        ServiceResult<ICollection<PermModel>> GetPerms(RoleModel role);

        /// <summary>
        /// Supprime la permission d'un rôle
        /// </summary>
        /// <param name="role"></param>
        /// <param name="perm"></param>
        /// <returns></returns>
        ServiceResult<int> RemovePerm(RoleModel role, string perm);

        /// <summary>
        /// Vérifie si un rôle existe
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        ServiceResult<bool> RoleExist(string role);
    }
}