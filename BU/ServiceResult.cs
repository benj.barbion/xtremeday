﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BU
{
    public enum ServiceResultStatus
    {
        Ok = 1,
        Ko = 2,
        NotFound = 3,
        BadRequest = 4,
        Unauthorized = 5,
        Forbidden = 6,
        AlreadyExist = 7
    }

    /// <summary>
    /// Classe permettant de gérer les résultats de services
    /// </summary>
    public class ServiceResult : IServiceResult
    {
        public ServiceResultStatus Status { get; set; }
        public object ObjectResult { get; set; }
        public string Message { get; set; }
    }

    /// <summary>
    /// Classe permettant de gérer les résultats de services, avec un typage fort d'objet
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ServiceResult<T>
    {
        public ServiceResultStatus Status { get; set; }
        public T ObjectResult { get; set; }
        public string Message { get; set; }

        /// <summary>
        /// Caste implicitement un ServiceResult en ServiceResult<T>
        /// </summary>
        /// <param name="v"></param>
        public static implicit operator ServiceResult<T>(ServiceResult v)
        {
            return new ServiceResult<T>()
            {
                Status = v.Status,
                ObjectResult = default,
                Message = v.Message
            };
        }
    }
}
