﻿using System;

namespace BU.Models
{
    public class RegistrationSearchFilterModel
    {
        public Guid? CodeTrainee { get; set; }
        public Guid? CodeRegistration { get; set; }
        public string EmailResponsable { get; set; }
        public StatusModel RegistrationStatus { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
    }
}
