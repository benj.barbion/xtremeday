﻿using System;
using System.Collections.Generic;

namespace BU.Models
{
    public class TraineeshipModel
    {
        public int Id { get; set; }
        public Guid? Code { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public short MaxInscription { get; set; }
        public decimal PricePerPerson { get; set; }
        public string Address { get; set; }
        public short RemainingInscription { get; set; }

        public EntityModel Entity { get; set; }
        public LevelModel Level { get; set; }
        public SportModel Sport { get; set; }
        public StatusModel Status { get; set; }
        public ICollection<ServiceModel> Services { get; set; } = new List<ServiceModel>();
    }
}
