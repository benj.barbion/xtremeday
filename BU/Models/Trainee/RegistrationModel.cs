﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BU.Models
{
    public class RegistrationModel
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public StatusModel Status { get; set; }
        public ClientModel Responsable { get; set; }
        public TraineeshipModel Traineeship { get; set; }
        public Guid? Code { get; set; }
        public ICollection<ClientModel> RegistrationClients { get; set; }
        public decimal Price => Traineeship?.PricePerPerson * RegistrationClients?.Count ?? 0;
    }
}
