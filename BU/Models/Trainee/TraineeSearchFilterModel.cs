﻿using System;
using System.Collections.Generic;

namespace BU.Models
{
    public class TraineeSearchFilterModel
    {
        public Guid? Code { get; set; }
        public StatusModel Status { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public short? RemainingInscription { get; set; }
        public ICollection<LevelModel> Levels { get; set; }
        public ICollection<SportModel> Sports { get; set; }
        public ICollection<EntityModel> Entities { get; set; }
        public ICollection<ServiceModel> Services { get; set; }
        public decimal? PriceFrom { get; set; }
        public decimal? PriceTo { get; set; }
    }
}
