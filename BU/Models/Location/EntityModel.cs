﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BU.Models
{
    public class EntityModel
    {
        public int Id { get; set; }
        public int PostalCode { get; set; }
        public string Name { get; set; }
        public int IdProvince { get; set; }

        public override string ToString()
        {
            return $"{this.PostalCode} - {this.Name}";
        }
    }
}
