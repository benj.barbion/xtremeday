﻿using System.Collections.Generic;

namespace BU.Models
{
    public class ProvinceModel
    {
        public ProvinceModel()
        {
            Entities = new HashSet<EntityModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<EntityModel> Entities { get; set; }
    }
}
