﻿namespace BU.Models
{
    public class StatusModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }

        public static implicit operator StatusModel(TraineeshipStatus status)
        {
            return new StatusModel()
            {
                Id = (int)status,
                Name = status.ToString()
            };
        }

        public static implicit operator StatusModel(RegistrationStatus status)
        {
            return new StatusModel()
            {
                Id = (int)status,
                Name = status.ToString()
            };
        }
    }
}
