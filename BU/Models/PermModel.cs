﻿namespace BU.Models
{
    public class PermModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
    }
}
