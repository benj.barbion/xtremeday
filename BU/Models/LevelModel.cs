﻿namespace BU.Models
{
    public class LevelModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }

        public static implicit operator LevelModel(BU.TraineeshipLevel level)
        {
            return new LevelModel()
            {
                Id = (int)level,
                Name = level.ToString()
            };
        }
    }
}
