﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BU.Models
{
    public class ClientModel
    {
        public int Id { get; set; }
        public PersonModel Person { get; set; } = new();

        public override string ToString()
        {
            return $"{Person.FirstName} {Person.LastName} | {Person.Email}";
        }

        public static implicit operator ClientModel(PersonModel v)
        {
            return new ClientModel()
            {
                Person = v 
            };
        }
    }
}
