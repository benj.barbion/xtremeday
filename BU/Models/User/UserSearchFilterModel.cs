﻿namespace BU.Models
{
    public class UserSearchFilterModel
    {
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
