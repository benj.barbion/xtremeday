﻿using System.Collections.Generic;

namespace BU.Models
{
    public class UserModel 
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public PersonModel Person { get; set; } = new();
        public ICollection<RoleModel> UserRoles { get; set; }

        public static implicit operator UserModel(PersonModel v)
        {
            return new UserModel()
            {
                Person = v
            };
        }
    }
}
