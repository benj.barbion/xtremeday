﻿using System;

namespace BU.Models
{
    public class PersonModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public DateTime Birtdhday { get; set; }
        public int IdEntity { get; set; }
        public EntityModel Entity { get; set; }

        public override string ToString() => $"{FirstName} {LastName} - Email : {Email} - Téléphone : {PhoneNumber}";
    }
}
