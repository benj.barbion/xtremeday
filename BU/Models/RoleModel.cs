﻿using System.Collections.Generic;

namespace BU.Models
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<PermModel> Perms { get; set; }
    }
}
