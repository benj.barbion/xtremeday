﻿using Caliburn.Micro;
using System.Threading.Tasks;

namespace UI.Helper
{
    public interface IModalHelper
    {
        Task CreateModal(IScreen screen, string title);
    }
}