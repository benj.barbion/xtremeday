﻿using Caliburn.Micro;
using System.Threading.Tasks;
using UI.Event;
using UI.Models;

namespace UI.Helper
{
    public class ModalHelper : IModalHelper
    {
        private readonly IEventAggregator eventAggregator;
        private readonly IEventFactory eventFactory;

        public ModalHelper(IEventAggregator eventAgg, IEventFactory eventFactory)
        {
            this.eventAggregator = eventAgg;
            this.eventFactory = eventFactory;
        }

        public async Task CreateModal(IScreen screen, string title)
        {
            var modal = this.eventFactory.CreateEvent(TypeOfEvent.OpenModalEvent);

            modal.ObjectResult = new ModalDisplayModel()
            {
                Screen = screen,
                Title = title
            };

            await this.eventAggregator.PublishOnUIThreadAsync(modal);
        }
    }
}
