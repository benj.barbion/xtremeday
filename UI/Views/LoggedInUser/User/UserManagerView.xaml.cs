﻿using System.Windows.Controls;

namespace UI.Views.LoggedInUser
{
    /// <summary>
    /// Logique d'interaction pour UserManagerView.xaml
    /// </summary>
    public partial class UserManagerView : UserControl
    {
        public UserManagerView()
        {
            InitializeComponent();
        }
    }
}
