﻿using System;

namespace UI.Event.Traineeship.Registration
{
    public class CreatorStepFinishedEvent : IEvent
    {
        public object ObjectResult { get; set; }
    }
}
