﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Event.Traineeship.Registration
{
    public class FinalizeStepFinishedEvent : IEvent
    {
        public object ObjectResult { get; set; }
    }
}
