﻿namespace UI.Event
{
    public class UpdateTraineeshipEvent : IEvent
    {
        public object ObjectResult { get; set; }
    }
}
