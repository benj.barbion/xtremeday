﻿namespace UI.Event
{
    public class OpenModalEvent : IEvent
    {
        public object ObjectResult { get; set; }
    }
}
