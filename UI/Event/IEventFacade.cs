﻿using System.Threading.Tasks;

namespace UI.Event
{
    public interface IEventFacade
    {
        Task PublishEvent(TypeOfEvent name, object objectResult = null);
    }
}