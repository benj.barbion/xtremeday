﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Event
{
    public class EventFacade : IEventFacade
    {
        readonly IEventAggregator eventAggregator;
        readonly IEventFactory eventFactory;

        public EventFacade(IEventAggregator eventAggregator, IEventFactory eventFactory)
        {
            this.eventAggregator = eventAggregator;
            this.eventFactory = eventFactory;
        }

        public async Task PublishEvent(TypeOfEvent name, object objectResult = null)
        {
            var pubEvent = this.eventFactory.CreateEvent(name, objectResult);

            if (pubEvent is not null)
                await this.eventAggregator.PublishOnUIThreadAsync(pubEvent);
        }
    }
}
