﻿using System;
using System.Collections.Generic;
using UI.Event.Authentication;
using UI.Event.Traineeship;
using UI.Event.Traineeship.Registration;

namespace UI.Event
{
    public enum TypeOfEvent
    {
        LogoutEvent,
        LoginEvent,
        RegisterUserToTraineeshipEvent,
        OpenModalEvent,
        CreatorStepFinishedEvent,
        ParticipantsStepFinishedEvent,
        ModifyTraineeshipEvent,
        NotificationEvent,
        FinalizeStepFinishedEvent,
        FindTraineeStepFinishedEvent,
        RefreshStatsEvent,
        SeeRegistrationFromTraineeEvent
    }

    public class EventFactory : IEventFactory
    {
        private readonly Dictionary<TypeOfEvent, IEvent> map;

        public EventFactory()
        {
            map = new Dictionary<TypeOfEvent, IEvent>
            {
                { TypeOfEvent.SeeRegistrationFromTraineeEvent, new SeeRegistrationFromTraineeEvent() },
                { TypeOfEvent.RefreshStatsEvent, new RefreshStatsEvent() },
                { TypeOfEvent.FindTraineeStepFinishedEvent, new FindTraineeStepFinishedEvent() },
                { TypeOfEvent.LogoutEvent, new LogoutEvent() },
                { TypeOfEvent.LoginEvent, new LoginEvent() },
                { TypeOfEvent.RegisterUserToTraineeshipEvent, new RegistrationToTraineeshipEvent() },
                { TypeOfEvent.ModifyTraineeshipEvent, new UpdateTraineeshipEvent() },
                { TypeOfEvent.OpenModalEvent, new OpenModalEvent() },
                { TypeOfEvent.CreatorStepFinishedEvent, new CreatorStepFinishedEvent() },
                { TypeOfEvent.ParticipantsStepFinishedEvent, new ParticipantsStepFinishedEvent() },
                { TypeOfEvent.NotificationEvent, new NotificationEvent() },
                { TypeOfEvent.FinalizeStepFinishedEvent, new FinalizeStepFinishedEvent() }
            };
        }

        public IEvent CreateEvent(TypeOfEvent type) => this.map.GetValueOrDefault(type);

        public IEvent CreateEvent(TypeOfEvent type, object data)
        {
            var eventObject = this.CreateEvent(type);
            if (eventObject is not null)
                eventObject.ObjectResult = data;
            return eventObject;
        }
    }
}
