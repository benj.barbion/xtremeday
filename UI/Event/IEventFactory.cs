﻿namespace UI.Event
{
    public interface IEventFactory
    {
        IEvent CreateEvent(TypeOfEvent type);
        IEvent CreateEvent(TypeOfEvent type, object data);
    }
}