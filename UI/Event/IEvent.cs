﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UI.Event
{
    public interface IEvent
    {
        public object ObjectResult { get; set; }
    }

    public interface IEvent<T>
    {
        public T ObjectResult { get; set; }
    }
}
