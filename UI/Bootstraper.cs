﻿using AutoMapper;
using Caliburn.Micro;
using UI.ViewModels;
using UI.Event;
using UI.Helper;
using HandyControl.Tools;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using BU.Models;
using UI.Models;
using BU.Services;
using DAL.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;
using DAL;
using Microsoft.EntityFrameworkCore;
using DAL.Repositories;
using BU.Services.User;
using UI.ApplicationLayer.Models;
using UI.ApplicationLayer.Services;
using Microsoft.Extensions.Logging;
using AutoMapper.Extensions.ExpressionMapping;

namespace UI
{
    public class Bootstraper : BootstrapperBase
    {
        private IServiceCollection services;
        
        public static IHost Host { get; private set; }

        public Bootstraper()
        {
            Host = Microsoft.Extensions.Hosting.Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) =>
                {
                    this.services = services;
                    ConfigureServices(this.services);
                })
                .ConfigureLogging(logging => logging.SetMinimumLevel(LogLevel.Warning))
                .Build();

            Initialize();

            ConfigHelper.Instance.SetLang("fr");
            ConventionManager.AddElementConvention<PasswordBox>(PasswordBoxHelper.BoundPasswordProperty, "Password", "PasswordChanged");
        }

        private static void DbFromOptions(IServiceProvider serviceProvider, DbContextOptionsBuilder options)
        {
            var connectionString = serviceProvider.GetService<IConfiguration>().GetConnectionString("DefaultConnection");           
            options.UseSqlServer(connectionString);
        }

        private void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.AddPooledDbContextFactory<XtremeDayContext>(DbFromOptions);

            //Register ViewModels
            GetType().Assembly
                .GetTypes()
                .Where(Predicate)
                .ToList()
                .ForEach(viewModelType => serviceCollection.AddTransient(viewModelType));

            serviceCollection
                .AddTransient(typeof(IGenericRepository<>), typeof(GenericRepository<>))
                .AddTransient<IEventFacade, EventFacade>()
                .AddTransient<IModalHelper, ModalHelper>()
                .AddTransient<IAuthenticationService, AuthenticationService>()
                .AddTransient<IEventFactory, EventFactory>()
                .AddTransient<EventFactory>();
                   
            serviceCollection
                .AddSingleton<IConfiguration>(AddConfiguration())
                .AddSingleton<IMapper>(ConfigureAutoMapper())
                .AddSingleton<IWindowManager, WindowManager>()
                .AddSingleton<IEventAggregator, EventAggregator>()
                .AddSingleton<LoggedInUserContext>()
                .AddSingleton<ShellViewModel>()
                .AddSingleton<IPermApplicationService, PermApplicationService>()
                .AddSingleton<IPersonService, PersonService>()
                .AddSingleton<IClientService, ClientService>()
                .AddSingleton<ISportService, SportService>()
                .AddSingleton<ITraineeshipService, BU.Services.TraineeshipService>()
                .AddSingleton<IFacilitiesService, FacilitiesService>()
                .AddSingleton<ILocationService, LocationService>()
                .AddSingleton<IUserService, UserService>()
                .AddSingleton<IRegistrationService, RegistrationService>()
                .AddSingleton<IRoleService, RoleService>();
        }

        private static bool Predicate(Type type) => type.IsClass && type.Name.EndsWith("ViewModel") && type.Name != "ShellViewModel";

        private static IMapper ConfigureAutoMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddExpressionMapping();

                cfg.CreateMap<EntityModel, EntityDisplayModel>().ReverseMap();
                cfg.CreateMap<PermModel, PermDisplayModel>().ReverseMap();
                cfg.CreateMap<RoleModel, RoleDisplayModel>().ReverseMap();
                cfg.CreateMap<SportModel, SportDisplayModel>().ReverseMap();
                cfg.CreateMap<ServiceModel, ServiceDisplayModel>().ReverseMap();
                cfg.CreateMap<LevelModel, LevelDisplayModel>().ReverseMap();
                cfg.CreateMap<RegistrationStatus, StatusModel>().ReverseMap();
                cfg.CreateMap<TraineeshipStatus, StatusModel>().ReverseMap();    
                cfg.CreateMap<Level, LevelModel>().ReverseMap();               
                cfg.CreateMap<UserModel, LoggedInUserContext>().ReverseMap();
                cfg.CreateMap<Province, ProvinceModel>().ReverseMap();
                cfg.CreateMap<Sport, SportModel>().ReverseMap();
                cfg.CreateMap<Perm, PermModel>().ReverseMap();
                cfg.CreateMap<Entity, EntityModel>().ReverseMap();
                cfg.CreateMap<Service, ServiceModel>().ReverseMap();
                cfg.CreateMap<ServiceModel, ServiceDisplayModel>().ReverseMap();
                cfg.CreateMap<Person, PersonModel>().ForMember(dest => dest.Entity, act => act.MapFrom(src => src.IdEntityNavigation)).ReverseMap();
                cfg.CreateMap<PersonModel, Person>()
                    .ForMember(dest => dest.IdEntity, act => act.MapFrom(src => src.Entity.Id))
                    .ForMember(dest => dest.Birthday, act => act.MapFrom(src => src.Birtdhday));
                
                cfg.CreateMap<Client, ClientModel>().ForMember(dest => dest.Person, act => act.MapFrom(src => src.IdPersonNavigation)).ReverseMap();

                cfg.CreateMap<Role, RoleModel>()
                    .ForMember(dest => dest.Perms, act => act.MapFrom(src => src.RolePerms.Select(rp => rp.IdPermNavigation)))
                    .ReverseMap();

                cfg.CreateMap<User, UserModel>()
                    .ForMember(dest => dest.Person, act => act.MapFrom(src => src.IdPersonNavigation))
                    .ForMember(dest => dest.UserRoles, act => act.MapFrom(src => src.UserRoles.Select(ur => ur.IdRoleNavigation)))
                    .ReverseMap();

                cfg.CreateMap<Traineeship, TraineeshipModel>()
                    .ForMember(dest => dest.Services, act => act.MapFrom(src => src.TraineeshipServices.Select(x => x.IdServiceNavigation)))
                    .ForMember(dest => dest.Status, act => act.MapFrom(src => src.IdTraineeshipStatusNavigation))
                    .ForMember(dest => dest.Entity, act => act.MapFrom(src => src.IdEntityNavigation))
                    .ForMember(dest => dest.Sport, act => act.MapFrom(src => src.IdSportNavigation))
                    .ForMember(dest => dest.Level, act => act.MapFrom(src => src.IdLevelNavigation));

                cfg.CreateMap<TraineeshipModel, Traineeship>()
                    .ForMember(dest => dest.TraineeshipServices, act => act.MapFrom(src => src.Services.Select(s => new DAL.Models.TraineeshipService(){IdService = s.Id})))
                    .ForMember(dest => dest.IdTraineeshipStatus, act => act.MapFrom(src => src.Status.Id))
                    .ForMember(dest => dest.IdEntity, act => act.MapFrom(src => src.Entity.Id))
                    .ForMember(dest => dest.IdSport, act => act.MapFrom(src => src.Sport.Id))
                    .ForMember(dest => dest.IdLevel, act => act.MapFrom(src => src.Level.Id));

                cfg.CreateMap<Registration, RegistrationModel>()
                    .ForMember(dest => dest.Id, act => act.MapFrom(src => src.Id))
                    .ForMember(dest => dest.Status, act => act.MapFrom(src => src.IdRegistrationStatusNavigation))
                    .ForMember(dest => dest.Traineeship, act => act.MapFrom(src => src.IdTraineeshipNavigation))
                    .ForMember(dest => dest.Responsable, act => act.MapFrom(src => src.IdResponsibleClientNavigation))
                    .ForMember(dest => dest.RegistrationClients, act => act.MapFrom(src => src.RegistrationClients.Select(rp => rp.IdClientNavigation)));

                cfg.CreateMap<RegistrationModel, Registration>()
                    .ForMember(dest => dest.IdRegistrationStatus, act => act.MapFrom(src => src.Status.Id))
                    .ForMember(dest => dest.IdResponsibleClient, act => act.MapFrom(src => src.Responsable.Id))
                    .ForMember(dest => dest.IdTraineeship, act => act.MapFrom(src => src.Traineeship.Id))
                    .ForMember(dest => dest.RegistrationClients, act => act.MapFrom(src => src.RegistrationClients.Select(rp => new RegistrationClient() { IdClient = rp.Id })));
            });

            return config.CreateMapper();
        }

        private static IConfiguration AddConfiguration()
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            return builder.Build();
        }

        protected override async void OnStartup(object sender, StartupEventArgs e) => await DisplayRootViewFor<ShellViewModel>();

        protected override object GetInstance(Type service, string key) => Host.Services.GetService(service);

        protected override IEnumerable<object> GetAllInstances(Type service) => new List<object>() { Host.Services.GetService(service) };

        protected override void OnExit(object sender, EventArgs e) => base.OnExit(sender, e);
    }
}
