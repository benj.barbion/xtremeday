﻿using BU.Extensions;
using BU.Models;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UI.Models
{
    public class PermDisplayModel : PermModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private bool isSelected; //Only need in UI for RoleManagerViewModel
       
        public bool IsSelected 
        {
            get => this.isSelected;
            set
            {
                this.isSelected = value;
                OnPropertyChanged();
                OnPropertyChanged("IsNotSelected");
            }
        } 
        public bool IsNotSelected => !IsSelected; 

        protected void OnPropertyChanged([CallerMemberName] string name = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        public static implicit operator PermDisplayModel(BU.Perms perm)
        {
            var permDisplay = new PermDisplayModel();
            permDisplay.Id = (int)perm;
            permDisplay.Name = perm.ToString();
            permDisplay.Desc = perm.GetDescription();
            return permDisplay;
        }
    }
}
