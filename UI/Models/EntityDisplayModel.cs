﻿using BU.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Models
{
    public class EntityDisplayModel : EntityModel
    {
        public bool Selected { get; set; } // only need in UI for checkcombobox
    }
}
