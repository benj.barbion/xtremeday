﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Models
{
    public class ModalDisplayModel
    {
        public String Title { get; set; }
        public IScreen Screen { get; set; }
    }
}
