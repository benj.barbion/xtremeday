﻿using BU.Models;

namespace UI.Models
{
    public class ServiceDisplayModel : ServiceModel
    {
        public bool Selected { get; set; } // only need in UI for checkcombobox
    }
}
