﻿using BU.Extensions;
using BU.Models;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UI.Models
{
    public class RoleDisplayModel : RoleModel
    {
        public bool Selected { get; set; } // only need in UI for checkcombobox
    }
}
