﻿using BU.Models;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace UI.Models
{
    public class SportDisplayModel : SportModel, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string name;

        public bool Selected { get; set; } // only need in UI for checkcombobox

        public override string Name 
        {
            get => this.name;
            set
            {
                this.name = value;
                OnPropertyChanged();
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string name = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
    }
}
