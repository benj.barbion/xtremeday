﻿using BU.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI.Models
{
    public class LevelDisplayModel : LevelModel
    {
        public bool Selected { get; set; } // only need in UI for checkcombobox

        public static implicit operator LevelDisplayModel(BU.TraineeshipLevel status)
        {
            return new LevelDisplayModel()
            {
                Id = (int)status,
                Name = status.ToString()
            };
        }
    }
}
