﻿using BU.Services;
using Caliburn.Micro;
using System;
using System.Threading;
using System.Threading.Tasks;
using UI.Event;

namespace UI.ViewModels
{
    public class StatisticsViewModel : Screen, IHandle<RefreshStatsEvent>
    {
        readonly ISportService sportService;
        readonly ITraineeshipService traineeshipService;
        readonly IRegistrationService registrationService;
        readonly IClientService clientService;

        public int TotalSports { get; private set; }
        public int TotalTrainee { get; private set; }
        public int TotalRegistrations { get; private set; }
        public int TotalClients { get; private set; }

        public StatisticsViewModel(IEventAggregator eventAgg, ISportService sportService, ITraineeshipService traineeshipService, IRegistrationService registrationService, IClientService clientService)
        {
            this.sportService = sportService;
            this.traineeshipService = traineeshipService;
            this.registrationService = registrationService;
            this.clientService = clientService;

            eventAgg.SubscribeOnUIThread(this);
            this.Load();
        }

        private void Load()
        {
            this.TotalSports = sportService?.Count() ?? 0;
            this.TotalTrainee = traineeshipService?.Count() ?? 0;
            this.TotalRegistrations = registrationService?.Count() ?? 0;
            this.TotalClients = clientService?.Count() ?? 0;
            this.Refresh();
        }

        public Task HandleAsync(RefreshStatsEvent message, CancellationToken cancellationToken)
        {
            this.Load();
            return Task.CompletedTask;
        }
    }
}
