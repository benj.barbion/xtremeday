﻿using Caliburn.Micro;
using System.Threading.Tasks;
using UI.Event;
using UI.Models;

namespace UI.ViewModels
{
    public class BaseViewModel : Conductor<IScreen>
    {
        readonly IEventFacade eventFacade;
       
        public BaseViewModel(IEventFacade eventFacade)
        {
            this.eventFacade = eventFacade;
        }

        public async Task SuccessNotification(string message = "Action effectuée avec succès.")
        {
            var display = new NotificationDisplayModel()
            {
                Title = message,
                Type = Notifications.Wpf.Core.NotificationType.Success
            };

            await this.eventFacade.PublishEvent(TypeOfEvent.NotificationEvent, display);
        }

        public async Task ErrorNotification(string message = "Une erreur s'est produite.")
        {
            var display = new NotificationDisplayModel()
            {
                Title = "Erreur",
                Message = message,
                Type = Notifications.Wpf.Core.NotificationType.Error
            };

            await this.eventFacade.PublishEvent(TypeOfEvent.NotificationEvent, display);
        }

        public async Task PublishEvent(TypeOfEvent name, object objectResult = null)
        {
            await this.eventFacade.PublishEvent(name, objectResult);
        }
    }
}
