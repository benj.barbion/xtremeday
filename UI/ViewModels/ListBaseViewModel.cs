﻿using BU.Extensions;
using BU.Models;
using BU.Services;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;
using UI.Event;

namespace UI.ViewModels
{
    public abstract class ListBaseViewModel<TModel> : BaseViewModel where TModel : class
    {
        Collection<TModel> models;
        string basicFilter;

        public IGenericService<TModel> GenericService { get; private set; }
        public int LoadPerPage { get; set; }
        public int Total { get; set; }
        public int TotalLoaded => this.Models?.Count ?? 0;
        public ICollectionView ModelsListView { get; set; }
        public TModel SelectedModel { get; set; }

        public ListBaseViewModel(IGenericService<TModel> genericService, IEventFacade eventFacade) : base(eventFacade)
        {
            this.GenericService = genericService;
            this.PopulateAndBind();
        }

        private void PopulateAndBind()
        {
            this.LoadPerPage = 20;
            this.Total = this.GenericService.Count();
            this.Models = new ObservableCollection<TModel>(this.GenericService.Take(this.LoadPerPage));
            this.ModelsListView = new ListCollectionView(this.Models) { Filter = this.FilterCollection };
            this.ModelsListView.MoveCurrentToPosition(-1);
            this.Refresh();
        }

        public void Reset()
        {
            this.Total = 0;
            this.Models.Clear();
            this.Refresh();
        }

        public Collection<TModel> Models
        {
            get => this.models;
            set => Set(ref this.models, value);
        }

        public string Filter
        {
            get => this.basicFilter;
            set
            {
                this.basicFilter = value;
                this.ModelsListView.Refresh();
                this.ModelsListView.MoveCurrentToPosition(-1);
            }
        }

        public abstract bool FilterCollection(object obj);

        public virtual void LoadMore()
        {
            this.GenericService.Take(this.LoadPerPage, this.TotalLoaded).Action(t => this.Models.Add(t));
            this.Refresh();
        }

        public bool CanLoadMore => this.TotalLoaded < this.Total;
    }
}
