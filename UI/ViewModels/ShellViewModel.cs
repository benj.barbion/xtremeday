﻿using Caliburn.Micro;
using UI.Event;
using UI.Models;
using System.Threading;
using System.Threading.Tasks;
using System.Dynamic;
using System.Windows;
using Notifications.Wpf.Core;

namespace UI.ViewModels
{
    public class ShellViewModel : Conductor<IScreen>.Collection.AllActive, IHandle<OpenModalEvent>, IHandle<NotificationEvent>
    {
        readonly IWindowManager windowManager;
        double opacity = 1.00;

        public ShellViewModel(IEventAggregator eventAgg, IWindowManager windowManager)
        {
            this.windowManager = windowManager;
            eventAgg.SubscribeOnPublishedThread(this);
        }

        /// <summary>
        /// EVENT : want to display a modal event
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task HandleAsync(OpenModalEvent message, CancellationToken cancellationToken)
        {
            if (message.ObjectResult is ModalDisplayModel modal)
            {
                this.Opacity = 0.2;

                dynamic settings = new ExpandoObject();
                settings.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                settings.ResizeMode = ResizeMode.NoResize;
                settings.Title = modal.Title;

                await this.windowManager.ShowDialogAsync(modal.Screen, null, settings);

                this.Opacity = 1;
            }
        }

        /// <summary>
        /// EVENT : triggered notification event
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task HandleAsync(NotificationEvent message, CancellationToken cancellationToken)
        {
            if (message.ObjectResult is NotificationDisplayModel notification)
            {
                var notificationManager = new NotificationManager();

                var notif = new NotificationContent
                {
                    Title = notification.Title,
                    Message = notification.Message,
                    Type = notification.Type
                };

                notificationManager.ShowAsync(notif, areaName: "WindowArea", token: cancellationToken);
            }

            return Task.CompletedTask;
        }

        public IScreen StatsViewModel => IoC.Get<StatisticsViewModel>();
        public IScreen UserStateView => IoC.Get<UserStateViewModel>();

        public double Opacity
        {
            get => this.opacity;
            set
            {
                this.opacity = value;
                NotifyOfPropertyChange(() => this.Opacity);
            }
        }
    }
}
