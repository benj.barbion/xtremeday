﻿using UI.Event;
using BU.Services.User;
using UI.ApplicationLayer.Models;
using AutoMapper;
using System.Threading.Tasks;

namespace UI.ViewModels
{
    public class LoggedOutViewModel : BaseViewModel
    {
        readonly IAuthenticationService xtremeDayAuthent;
        string username;
        string password;
        bool canLogin;

        public LoggedOutViewModel(IAuthenticationService xtremeDayAuthent, IEventFacade eventFacade) : base(eventFacade)
        {
            this.xtremeDayAuthent = xtremeDayAuthent;
        }

        public bool CanLogin
        {
            get => this.canLogin;
            set => Set(ref this.canLogin, value);
        }

        public async Task Login()
        {
            var user = this.xtremeDayAuthent.AuthenticateUser(this.Username, this.Password);

            if (user.Status == BU.ServiceResultStatus.Ok)
                await this.PublishEvent(TypeOfEvent.LoginEvent, user.ObjectResult);
            else
                await this.ErrorNotification(user.Message);
        }

        public string Username
        {
            get => this.username;
            set
            {
                this.username = value;
                this.CanLogin = CheckBoxLength;
            }
        }

        public string Password
        {
            get => this.password;
            set
            {
                this.password = value;
                this.CanLogin = CheckBoxLength;
            }
        }

        private bool CheckBoxLength => this.Username?.Length > 2 && this.Password?.Length > 2;
    }
}
