﻿using BU.Models;
using Caliburn.Micro;
using System.Threading;
using System.Threading.Tasks;
using UI.ApplicationLayer.Models;
using UI.Event;
using UI.Event.Authentication;
using UI.ViewModels.LoggedInUser;

namespace UI.ViewModels
{
    public class UserStateViewModel : BaseViewModel, IHandle<LoginEvent>, IHandle<LogoutEvent>
    {
        readonly LoggedInUserContext loggedInUserContext;

        public UserStateViewModel(LoggedInUserContext loggedInUserContext, IEventAggregator eventAgg, IEventFacade facade) : base(facade)
        {
            this.loggedInUserContext = loggedInUserContext;
            eventAgg.SubscribeOnPublishedThread(this);
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            this.LoggedOutUserAsync();
        }

        public Task LoggedOutUserAsync()
        {
            ActivateItemAsync(IoC.Get<LoggedOutViewModel>());
            return Task.CompletedTask;
        }

        private Task LoggedInUserAsync()
        {
            ActivateItemAsync(IoC.Get<LoggedInViewModel>());
            return Task.CompletedTask;
        }

        public async Task HandleAsync(LoginEvent message, CancellationToken cancellationToken)
        {
            var user = message.ObjectResult as UserModel;
            if (user is null)
            {
                await this.ErrorNotification("Erreur inconnuee connexion.");
                return;
            }
                
            this.loggedInUserContext.Create(user);
            await this.LoggedInUserAsync();
        }

        public async Task HandleAsync(LogoutEvent message, CancellationToken cancellationToken)
        {
            this.loggedInUserContext.Reset();
            await this.LoggedOutUserAsync();
        }
    }
}
