﻿using AutoMapper;
using BU.Models;
using BU.Services;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UI.Event;
using UI.Models;

namespace UI.ViewModels.LoggedInUser
{
    public class SportsViewModel : BaseViewModel
    {
        readonly ISportService sportService;
        readonly IMapper mapper;
        SportModel selectedSport;
        string newSportName;
        string modifiedSportName;

        public Collection<SportModel> Sports { get; set; }

        public SportsViewModel(ISportService sportService, IMapper mapper, IEventFacade notificationHelper) : base(notificationHelper)
        {
            this.sportService = sportService;
            this.mapper = mapper;
            this.Sports = new ObservableCollection<SportModel>();
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            var displaySports = this.mapper.Map<ICollection<SportDisplayModel>>(this.sportService.GetAll());
            this.Sports = new ObservableCollection<SportModel>(displaySports);
            NotifyOfPropertyChange(() => this.Sports);
        }

        public bool CanAddSport => this.NewSportName?.Length > 3;
        
        public async Task AddSport()
        {
            var success = this.sportService.Create(this.NewSportName);
            if (success.Status == BU.ServiceResultStatus.Ok)
            {
                this.Sports.Insert(0, success.ObjectResult);
                this.NewSportName = "";
                await this.SuccessNotification();
                await this.PublishEvent(TypeOfEvent.RefreshStatsEvent);
            }
            else
                await this.ErrorNotification(success.Message);
        }

        public bool CanModifySport => this.selectedSport is not null && this.SelectedSport.Name != this.ModifiedSportName && this.ModifiedSportName?.Length > 3;
        
        public async Task ModifySport()
        {
            var oldName = new string(this.SelectedSport.Name);

            this.SelectedSport.Name = this.ModifiedSportName;
            
            var update = this.sportService.Update(this.SelectedSport);

            if (update.Status == BU.ServiceResultStatus.Ok)
            {
                this.SelectedSport.Name = this.ModifiedSportName;
                this.ModifiedSportName = "";
                await this.SuccessNotification();
            }
            else
            {
                await this.ErrorNotification(update.Message);
                this.SelectedSport.Name = oldName;
            }             
        }

        public SportModel SelectedSport
        {
            get => this.selectedSport;
            set
            {
                Set(ref this.selectedSport, value);
                this.ModifiedSportName = value.Name;
            }
        }

        public string NewSportName
        {
            get => this.newSportName;
            set
            {
                Set(ref newSportName, value);
                NotifyOfPropertyChange(() => this.CanAddSport);
            }
        }

        public string ModifiedSportName
        {
            get => this.modifiedSportName;
            set
            {
                Set(ref modifiedSportName, value);
                NotifyOfPropertyChange(() => this.CanModifySport);
            }
        }
    }
}
