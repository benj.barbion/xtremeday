﻿using Caliburn.Micro;
using UI.Event;
using UI.ViewModels.LoggedInUser.Traineeship;
using System.Threading;
using System.Threading.Tasks;
using BU.Models;
using UI.ViewModels.LoggedInUser.Traineeship.Registration;
using System;
using Microsoft.Extensions.DependencyInjection;
using UI.ApplicationLayer.Services;
using UI.Event.Traineeship;

namespace UI.ViewModels.LoggedInUser
{
    public class LoggedInViewModel : BaseViewModel, IHandle<RegistrationToTraineeshipEvent>, IHandle<UpdateTraineeshipEvent>, IHandle<SeeRegistrationFromTraineeEvent>
    {
        readonly IServiceProvider serviceProvider;
        readonly IPermApplicationService permApplicationService;

        public LoggedInViewModel(IEventAggregator eventAgg, IServiceProvider serviceProvider, IPermApplicationService permissionService, IEventFacade eventFacade) : base(eventFacade)
        {
            this.serviceProvider = serviceProvider;
            this.permApplicationService = permissionService;
            eventAgg.SubscribeOnPublishedThread(this);   
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            this.Home();
        }

        public bool CanHome => ActiveItem is not ListTraineeshipViewModel;
        public bool CanRegistration => ActiveItem is not RegistrationToTraineeshipViewModel;
        public bool CanCreateTraineeship => ActiveItem is not CreateTraineeshipViewModel;
        public bool CanAdvancedSearch => ActiveItem is not AdvancedSearchViewModel;
        public bool CanServicesManager => ActiveItem is not FacilitiesViewModel;
        public bool CanSportsManager => ActiveItem is not SportsViewModel;
        public bool CanEntitiesManager => ActiveItem is not LocationViewModel;
        public bool CanListOfRegistrations => ActiveItem is not ListOfRegistrationsViewModel;
        public bool CanListOfClients => ActiveItem is not ClientManagerViewModel;
        public bool CanManageUser => this.permApplicationService.HasPerm(BU.Perms.canManageUser);
        public bool CanManagePerm => this.permApplicationService.HasPerm(BU.Perms.canManagePerm);
        public bool CanManageSport => this.permApplicationService.HasPerm(BU.Perms.canManageSport);
        public bool CanManageService => this.permApplicationService.HasPerm(BU.Perms.canManageService);
        public bool CanManageEntity => this.permApplicationService.HasPerm(BU.Perms.canManageEntity);

        public void Home() => this.NotifyAllFrom(serviceProvider.GetService<ListTraineeshipViewModel>());
        public void Registration() => this.NotifyAllFrom(serviceProvider.GetService<RegistrationToTraineeshipViewModel>());
        public void CreateTraineeship() => this.NotifyAllFrom(serviceProvider.GetService<CreateTraineeshipViewModel>());
        public void AdvancedSearch() => this.NotifyAllFrom(serviceProvider.GetService<AdvancedSearchViewModel>());
        public void SportsManager() => this.NotifyAllFrom(this.serviceProvider.GetService<SportsViewModel>());
        public void ServicesManager() => this.NotifyAllFrom(this.serviceProvider.GetService<FacilitiesViewModel>());
        public void EntitiesManager() => this.NotifyAllFrom(this.serviceProvider.GetService<LocationViewModel>());
        public void MyAccount() => this.NotifyAllFrom(this.serviceProvider.GetService<MyAccountViewModel>());
        public void ListOfRegistrations() => this.NotifyAllFrom(serviceProvider.GetService<ListOfRegistrationsViewModel>());
        public void ManageUser() => this.NotifyAllFrom(this.serviceProvider.GetService<UserManagerViewModel>());
        public void ManagePerm() => this.NotifyAllFrom(this.serviceProvider.GetService<RoleManagerViewModel>());
        public void ListOfClients() => this.NotifyAllFrom(this.serviceProvider.GetService<ClientManagerViewModel>());

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task HandleAsync(UpdateTraineeshipEvent message, CancellationToken cancellationToken)
        {
            var selectedTrainee = message.ObjectResult as TraineeshipModel;
            if (selectedTrainee is null)
            {
                await this.ErrorNotification("Pas de stage sélectionné.");
                return;
            }

            var createTraineeship = serviceProvider.GetService<CreateTraineeshipViewModel>();           
            createTraineeship.UpdateTraineeship(selectedTrainee);
            this.NotifyAllFrom(createTraineeship);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task HandleAsync(RegistrationToTraineeshipEvent message, CancellationToken cancellationToken)
        {
            var registration = serviceProvider.GetService<RegistrationToTraineeshipViewModel>();
            registration.SelectedTraineeship = message.ObjectResult as TraineeshipModel;
            this.NotifyAllFrom(registration);           
            return Task.CompletedTask;
        }

        public async Task Logout()
        {
            await this.PublishEvent(TypeOfEvent.LogoutEvent);
        }

        private void NotifyAllFrom(IScreen screenToActivate)
        {
            this.ActivateItemAsync(screenToActivate);
            this.Refresh();
        }

        public Task HandleAsync(SeeRegistrationFromTraineeEvent message, CancellationToken cancellationToken)
        {
            var registration = serviceProvider.GetService<ListOfRegistrationsViewModel>();
            this.NotifyAllFrom(registration);
            registration.ExecuteFilter(new RegistrationSearchFilterModel() { CodeTrainee = message.ObjectResult as Guid? });
            return Task.CompletedTask;
        }
    }
}
