﻿using AutoMapper;
using BU.Extensions;
using BU.Models;
using BU.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using UI.Event;
using UI.Models;

namespace UI.ViewModels.LoggedInUser.Traineeship
{
    public class AdvancedSearchViewModel : BaseViewModel
    {
        readonly IMapper mapper;
        string message;
        
        public TraineeSearchFilterModel SearchFilterModel { get; private set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public ICollection<ServiceDisplayModel> ListOfServices { get; set; }
        public ICollection<EntityDisplayModel> ListOfEntities { get; set; }
        public ICollection<SportDisplayModel> ListOfSports { get; set; }
        public ICollection<LevelDisplayModel> ListOfLevels { get; set; }
        public ICollection<StatusModel> ListOfStatus { get; set; }
        
        public AdvancedSearchViewModel(IMapper mapper, ISportService sportService, IFacilitiesService serviceService, ILocationService locationService, IEventFacade eventFacade) : base(eventFacade)
        {
            this.mapper = mapper;

            this.Message = "Remplissez les champs que vous désirez pour la recherche.";
            this.SearchFilterModel = new();
            
            this.ListOfSports = mapper.Map<ICollection<SportDisplayModel>>(sportService.GetAll());
            this.ListOfServices = mapper.Map<List<ServiceDisplayModel>>(serviceService.GetAll());
            this.ListOfEntities = mapper.Map<List<EntityDisplayModel>>(locationService.GetAll());

            this.ListOfStatus = new List<StatusModel>();
            Enum.GetValues(typeof(BU.TraineeshipStatus)).Cast<BU.TraineeshipStatus>().Action(x => this.ListOfStatus.Add(x));

            this.ListOfLevels = new List<LevelDisplayModel>(); 
            Enum.GetValues(typeof(BU.TraineeshipLevel)).Cast<BU.TraineeshipLevel>().Action(x => this.ListOfLevels.Add(x));
        }

        public async Task Save()
        {
            var selectedServices = this.ListOfServices.Where(s => s.Selected);
            var selectedEntities = this.ListOfEntities.Where(s => s.Selected);
            var selectedSports = this.ListOfSports.Where(s => s.Selected);
            var selectedLevels = this.ListOfLevels.Where(s => s.Selected);       
            var startDateVerif = DateTime.TryParseExact(DateFrom, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out var startDate);
            var endDateVerif = DateTime.TryParseExact(DateTo, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out var endDate);
            
            this.SearchFilterModel.Services = mapper.Map<ICollection<ServiceModel>>(selectedServices);
            this.SearchFilterModel.Sports = mapper.Map<ICollection<SportModel>>(selectedSports);
            this.SearchFilterModel.Levels = mapper.Map<ICollection<LevelModel>>(selectedLevels);
            this.SearchFilterModel.Entities = mapper.Map<ICollection<EntityModel>>(selectedEntities);
            this.SearchFilterModel.DateFrom = startDateVerif && startDate > DateTime.MinValue ? startDate : null;
            this.SearchFilterModel.DateTo = endDateVerif && endDate > DateTime.MinValue ? endDate : null;

            await TryCloseAsync(true);
        }

        public string Message
        {
            get => this.message;
            set => Set(ref this.message, value);
        }
    }
}
