﻿using BU.Models;
using BU.Services;
using Caliburn.Micro;
using System.Linq;
using System.Threading.Tasks;
using UI.Event;
using UI.Models;

namespace UI.ViewModels.LoggedInUser.Traineeship.Registration
{
    public class FinalizeStepViewModel : BaseViewModel
    {
        readonly IRegistrationService registrationService;

        public RegistrationModel Registration { get; set; }

        public FinalizeStepViewModel(IRegistrationService registrationService, IEventFacade eventFacade) : base(eventFacade)
        {
            this.registrationService = registrationService;
        }

        public async Task EndRegistration()
        {
            var result = this.registrationService.Create(Registration);
            
            if (result.Status == BU.ServiceResultStatus.Ok)
            {
                await this.PublishEvent(TypeOfEvent.FinalizeStepFinishedEvent);
                await this.SuccessNotification("Inscription terminée avec succès !");
            }
            else
                await this.ErrorNotification(result.Message);
        }
    }
}
