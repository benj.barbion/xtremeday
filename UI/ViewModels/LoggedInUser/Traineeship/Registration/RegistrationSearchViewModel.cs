﻿using BU.Extensions;
using BU.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using UI.Event;

namespace UI.ViewModels.LoggedInUser.Traineeship.Registration
{
    public class RegistrationSearchViewModel : BaseViewModel
    {
        public ICollection<StatusModel> ListOfStatus { get; set; }
        public RegistrationSearchFilterModel SearchFilterModel { get; private set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }

        public RegistrationSearchViewModel(IEventFacade eventFacade) : base(eventFacade)
        {
            this.SearchFilterModel = new();
            this.ListOfStatus = new List<StatusModel>();
            Enum.GetValues(typeof(BU.RegistrationStatus))
                .Cast<BU.RegistrationStatus>()
                .Action(x => this.ListOfStatus.Add(x));
        }

        public async Task Save()
        {
            var startDateVerif = DateTime.TryParseExact(DateFrom, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out var startDate);
            var endDateVerif = DateTime.TryParseExact(DateTo, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out var endDate);
            this.SearchFilterModel.From = startDateVerif && startDate > DateTime.MinValue ? startDate : null;
            this.SearchFilterModel.To = endDateVerif && endDate > DateTime.MinValue ? endDate : null;
            await TryCloseAsync(true);
        }
    }
}
