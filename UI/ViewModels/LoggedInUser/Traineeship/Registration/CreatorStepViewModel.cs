﻿using BU.Models;
using BU.Services;
using Caliburn.Micro;
using System.Collections.Generic;
using System.Threading.Tasks;
using UI.Event;
using UI.Helper;

namespace UI.ViewModels.LoggedInUser.Traineeship.Registration
{
    public class CreatorStepViewModel : BaseViewModel
    {
        readonly IModalHelper modalHelper;
        readonly IClientService clientService;
        ClientModel selectedClient;

        public ICollection<ClientModel> Clients { get; set; }

        public CreatorStepViewModel(IModalHelper modalHelper, IClientService userService, IEventFacade eventFacade) : base(eventFacade)
        {
            this.modalHelper = modalHelper;
            this.clientService = userService;
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            this.Clients = new List<ClientModel>(clientService.GetAll());
            NotifyOfPropertyChange(() => this.Clients); 
        }

        public ClientModel SelectedClient
        {
            get => this.selectedClient;
            set
            {
                this.selectedClient = value;
                NotifyOfPropertyChange(() => this.CanAddTraineeManager);
            }
        }

        public async Task AddStagiaire()
        {
            var client = IoC.Get<AddParticipantViewModel>();
            await this.modalHelper.CreateModal(client, "Ajouter un stagiaire");
            if (client.Model?.Person?.Email is not null)
            {
                this.SelectedClient = client.Model;
                await this.AddTraineeManager();
            }           
        }

        public bool CanAddTraineeManager => this.SelectedClient is not null;

        public async Task AddTraineeManager()
        {
            await this.PublishEvent(TypeOfEvent.CreatorStepFinishedEvent, SelectedClient);
        }
    }
}