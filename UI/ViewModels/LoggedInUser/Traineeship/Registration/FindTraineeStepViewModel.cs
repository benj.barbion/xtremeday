﻿using BU;
using BU.Models;
using BU.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using UI.Event;

namespace UI.ViewModels.LoggedInUser.Traineeship.Registration
{
    public class FindTraineeStepViewModel : BaseViewModel
    {
        readonly ITraineeshipService traineeshipService;
        TraineeshipModel selectedTrainee;

        public ICollection<TraineeshipModel> Traineeships { get; private set; }

        public FindTraineeStepViewModel(IEventFacade eventFacade, ITraineeshipService traineeshipService) : base(eventFacade)
        {
            this.traineeshipService = traineeshipService;
        }

        protected override void OnViewLoaded(object view)
        {
            // we can also use TraineeSearchFilterModel if we want to (+ predicate can be in BU layer)
            var predicate = traineeshipService.Find((TraineeshipModel t) => t.Status.Id == (int)TraineeshipStatus.OUVERT && t.RemainingInscription > 0);
            this.Traineeships = new List<TraineeshipModel>(predicate);
            NotifyOfPropertyChange(() => this.Traineeships);
        }

        public TraineeshipModel SelectedTrainee
        {
            get => this.selectedTrainee;
            set
            {
                this.selectedTrainee = value;
                NotifyOfPropertyChange(() => this.CanNextStep);
            }
        }

        public bool CanNextStep => this.SelectedTrainee is not null;

        public async Task NextStep()
        {
            await this.PublishEvent(TypeOfEvent.FindTraineeStepFinishedEvent, this.SelectedTrainee);
        }
    }
}
