﻿using Caliburn.Micro;
using UI.Event;
using BU.Services;
using UI.Helper;
using BU.Models;
using BU.Extensions;
using System.Threading.Tasks;
using UI.ViewModels.LoggedInUser.Traineeship.Registration;
using BU;

namespace UI.ViewModels.LoggedInUser.Traineeship
{
    public class ListOfRegistrationsViewModel : ListBaseViewModel<RegistrationModel>
    {
        readonly IModalHelper modalHelper;

        public RegistrationSearchFilterModel AdvancedFilter { get; set; }

        public ListOfRegistrationsViewModel(IEventAggregator eventAggregator, IRegistrationService service, IEventFacade eventFacade, IModalHelper modalHelper) : base(service, eventFacade)
        {
            this.modalHelper = modalHelper;
            eventAggregator.SubscribeOnUIThread(this);
        }

        public override void LoadMore()
        {
            this.Service.Take(this.LoadPerPage, this.TotalLoaded, this.AdvancedFilter).Action(t => this.Models.Add(t));
            this.Refresh();
        }

        public override bool FilterCollection(object obj)
        {
            if (this.Filter is null)
                return true;

            var registration = obj as RegistrationModel;
            if (registration == null)
                return true;

            if (registration.Traineeship?.Title?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (registration.Responsable?.Person?.Email?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            return false;
        }

        public async Task AdvancedSearch()
        {
            var advancedSearch = IoC.Get<RegistrationSearchViewModel>();
            await this.modalHelper.CreateModal(advancedSearch, "Recherches avancées");
            this.ExecuteFilter(advancedSearch.SearchFilterModel);
        }

        public bool CanValidateRegistration => this.SelectedModel is not null && this.SelectedModel.Status.Id == (int)RegistrationStatus.OUVERT;

        public async Task ValidateRegistration()
        {
            var result = this.Service.ValidateRegistration(this.SelectedModel.Id);
            if (result.Status == ServiceResultStatus.Ok)
            {
                this.SelectedModel.Status.Id = (int)RegistrationStatus.VALIDE;
                await this.SuccessNotification();
                this.Refresh();
            }
            else
                await this.ErrorNotification(result.Message);
        }

        public bool CanCancelRegistration => this.SelectedModel is not null && this.SelectedModel.Status.Id != (int)RegistrationStatus.ANNULE;

        public async Task CancelRegistration()
        {
            var result = this.Service.CancelRegistration(this.SelectedModel.Id);
            if (result.Status == ServiceResultStatus.Ok)
            {
                this.SelectedModel.Status.Id = (int)RegistrationStatus.ANNULE;
                await this.SuccessNotification();
                this.Refresh();
            }
            else
                await this.ErrorNotification(result.Message);
        }

        public void ExecuteFilter(RegistrationSearchFilterModel filter)
        {
            this.AdvancedFilter = filter;
            this.Reset();
            this.Total = this.Service.Count(AdvancedFilter);
            this.LoadMore();
        }

        private IRegistrationService Service => this.GenericService as IRegistrationService;
    }
}