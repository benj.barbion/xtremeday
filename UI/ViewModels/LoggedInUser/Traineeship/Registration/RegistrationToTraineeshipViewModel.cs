﻿using BU.Models;
using BU.Services;
using Caliburn.Micro;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UI.Event;
using UI.Event.Traineeship.Registration;

namespace UI.ViewModels.LoggedInUser.Traineeship.Registration
{
    public class RegistrationToTraineeshipViewModel : BaseViewModel, IHandle<FindTraineeStepFinishedEvent>, IHandle<FinalizeStepFinishedEvent>, IHandle<CreatorStepFinishedEvent>, IHandle<ParticipantsStepFinishedEvent>
    {
        FindTraineeStepViewModel traineeStep; // step 1
        CreatorStepViewModel creatorStep; // step 2
        ParticipantsStepViewModel participantsStep; // step 3
        FinalizeStepViewModel finalizeStep; // step 4
        int stepIndex;

        public TraineeshipModel SelectedTraineeship { get; set; }

        public RegistrationToTraineeshipViewModel(IEventAggregator eventAgg, IEventFacade eventFacade) : base(eventFacade)
        {
            eventAgg.SubscribeOnPublishedThread(this);
            this.PopulateAndBind();
        }

        private void PopulateAndBind()
        {
            this.traineeStep = IoC.Get<FindTraineeStepViewModel>();
            this.creatorStep = IoC.Get<CreatorStepViewModel>();
            this.participantsStep = IoC.Get<ParticipantsStepViewModel>();
            this.finalizeStep = IoC.Get<FinalizeStepViewModel>();
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            if (this.SelectedTraineeship is not null)
                this.CreatorStep();
            else
                this.TraineeStep();
        }

        /// <summary>
        /// EVENT : the user founded the right trainee, we have to go to CreatorStep
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task HandleAsync(FindTraineeStepFinishedEvent message, CancellationToken cancellationToken)
        {
            this.SelectedTraineeship = this.traineeStep.SelectedTrainee;
            this.CreatorStep();
            return Task.CompletedTask;
        }

        /// <summary>
        /// EVENT : the user founded the right creator, we have to go to ParticipantStep
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task HandleAsync(CreatorStepFinishedEvent message, CancellationToken cancellationToken)
        {
            this.ParticipantsStep();
            return Task.CompletedTask;
        }

        /// <summary>
        /// EVENT : the user finished to insert participants
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task HandleAsync(ParticipantsStepFinishedEvent message, CancellationToken cancellationToken)
        {
            this.FinalizeStep();
            return Task.CompletedTask;
        }

        /// <summary>
        /// EVENT : registration finished
        /// </summary>
        /// <param name="message"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task HandleAsync(FinalizeStepFinishedEvent message, CancellationToken cancellationToken)
        {
            this.SelectedTraineeship = null;
            this.PopulateAndBind();
            this.TraineeStep();
            return Task.CompletedTask;
        }

        private void TraineeStep()
        {
            this.StepIndex = 1;
            ActivateItemAsync(this.traineeStep);
        }

        public bool CanCreatorStep => this.StepIndex > 1;
        private void CreatorStep()
        {
            this.StepIndex = 2;
            ActivateItemAsync(this.creatorStep);
        }

        public bool CanParticipantsStep => this.StepIndex > 2;
        private void ParticipantsStep()
        {
            this.StepIndex = 3;
            this.participantsStep.Responsable = this.creatorStep.SelectedClient;
            this.participantsStep.Clients = this.creatorStep.Clients; // don't load the clients again !
            this.participantsStep.Traineeship = this.SelectedTraineeship;
            ActivateItemAsync(this.participantsStep);
        }

        public bool CanFinalizeStep => this.StepIndex > 3;
        private void FinalizeStep()
        {
            this.StepIndex = 4;

            var registration = new RegistrationModel()
            {
                Traineeship = this.SelectedTraineeship,
                RegistrationClients = this.participantsStep.SelectedClients,
                Responsable = this.participantsStep.Responsable
            };

            this.finalizeStep.Registration = registration;
            ActivateItemAsync(this.finalizeStep);
        }

        public int StepIndex
        {
            get => this.stepIndex - 1;
            set 
            {
                this.stepIndex = value;
                this.Refresh();
            }
        }
    }
}
