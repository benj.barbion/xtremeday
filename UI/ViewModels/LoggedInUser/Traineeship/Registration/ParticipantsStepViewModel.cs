﻿using BU.Models;
using BU.Services;
using Caliburn.Micro;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using UI.Event;
using UI.Helper;

namespace UI.ViewModels.LoggedInUser.Traineeship.Registration
{
    public class ParticipantsStepViewModel : BaseViewModel
    {
        readonly IModalHelper modalHelper;
        bool toggleManager;
        ClientModel clientToAdd;

        public ICollection<ClientModel> Clients { get; set; }
        public ClientModel Responsable { get; set; }
        public TraineeshipModel Traineeship { get; set; }
        public ICollection<ClientModel> SelectedClients { get; set; }
        public ClientModel SelectedClient { get; set; }

        public ParticipantsStepViewModel(IModalHelper modalHelper, IEventFacade eventFacade) : base(eventFacade)
        {
            this.modalHelper = modalHelper;
            this.SelectedClients = new ObservableCollection<ClientModel>();
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            this.ToggleManager = true;
        }

        public void DeleteClient()
        {
            this.SelectedClients.Remove(this.SelectedClient);
            NotifyOfPropertyChange(() => this.CanFinished);
        }

        public bool CanFinished => this.SelectedClients.Count > 0;

        public async Task Finished()
        {
            await this.PublishEvent(TypeOfEvent.ParticipantsStepFinishedEvent);
        }

        public bool CanAddClient => this.ClientToAdd is not null;

        public async Task AddClient()
        {
            // we can also imagine to use predicate logical if we were using a simple Textbox
            // var user = this.clientService.Find((ClientModel p) => p.Person.PhoneNumber == this.SearchBox || p.Person.Email == this.SearchBox);
            
            // also checked inside BU layer
            if (Traineeship.RemainingInscription < this.SelectedClients.Count+1)
            {
                await this.ErrorNotification("Il n'y a plus assez de places dans ce stage pour ajouter un membre supplémentaire.");
                return;
            }

            if (this.SelectedClients.Any(p => p.Id == this.ClientToAdd.Id))
            {
                await this.ErrorNotification("Le membre est déjà dans la liste.");
                return;
            }

            this.SelectedClients.Add(this.ClientToAdd);
            NotifyOfPropertyChange(() => this.CanFinished);
        }

        public async Task AddClientModal()
        {
            var client = IoC.Get<AddParticipantViewModel>();
            await this.modalHelper.CreateModal(client, "Ajouter un stagiaire");
            if (client.Model?.Person?.Email is not null)
            {
                this.SelectedClients.Add(client.Model);
                NotifyOfPropertyChange(() => this.CanFinished);
            }               
        }

        public ClientModel ClientToAdd
        {
            get => this.clientToAdd;
            set
            {
                this.clientToAdd = value;
                NotifyOfPropertyChange(() => CanAddClient);
            }
        }

        public bool ToggleManager
        {
            get => this.toggleManager;
            set
            {
                Set(ref this.toggleManager, value);

                if (value is true)
                    this.SelectedClients.Add(Responsable);
                else
                    this.SelectedClients.Remove(Responsable);

                NotifyOfPropertyChange(() => CanFinished);
            }
        }
    }
}
