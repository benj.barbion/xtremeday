﻿using BU.Models;
using BU.Services;
using Caliburn.Micro;
using UI.Event;
using BU.Extensions;
using UI.Helper;
using System.Threading.Tasks;
using BU;

namespace UI.ViewModels.LoggedInUser.Traineeship
{
    public class ListTraineeshipViewModel : ListBaseViewModel<TraineeshipModel>
    {
        readonly IModalHelper modalHelper;
        TraineeSearchFilterModel advancedFilter;
       
        public ListTraineeshipViewModel(IEventAggregator eventAggregator, ITraineeshipService traineeshipService, IEventFacade eventFacade, IModalHelper modalHelper) : base(traineeshipService, eventFacade)
        {
            eventAggregator.SubscribeOnUIThread(this);
            this.modalHelper = modalHelper;
        }

        public override bool FilterCollection(object obj)
        {
            if (this.Filter is null || obj is not TraineeshipModel traineeship)
                return true;

            if (traineeship.Level?.Name?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (traineeship.Status?.Name?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (traineeship.Title?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (traineeship.Code?.ToString().ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (traineeship.Entity?.Name?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (traineeship.Sport?.Name?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            return false;
        }

        public async Task AdvancedSearch()
        {
            var advancedSearch = IoC.Get<AdvancedSearchViewModel>();           
            await this.modalHelper.CreateModal(advancedSearch, "Recherches avancées");
            this.advancedFilter = advancedSearch.SearchFilterModel;          
            this.Reset();
            this.Total = this.TraineeshipService.Count(advancedFilter);
            this.LoadMore();  
        }

        public override void LoadMore()
        {
            this.TraineeshipService
                .Take(this.LoadPerPage, this.TotalLoaded, this.advancedFilter)
                .Action(t => this.Models.Add(t));
            
            this.Refresh();
        }

        public bool CanAddParticipant => this.SelectedModel?.RemainingInscription > 0 && this.SelectedModel.Status.Id == (int)TraineeshipStatus.OUVERT;

        public async Task AddParticipant()
        {
            await base.PublishEvent(TypeOfEvent.RegisterUserToTraineeshipEvent, this.SelectedModel);
        }

        public async Task SeeParticipants()
        {
            await base.PublishEvent(TypeOfEvent.SeeRegistrationFromTraineeEvent, this.SelectedModel.Code);
        }

        public bool CanModifyTraineeship => this.SelectedModel?.Status.Id == (int)TraineeshipStatus.OUVERT;
        public async Task ModifyTraineeship()
        {
            await base.PublishEvent(TypeOfEvent.ModifyTraineeshipEvent, this.SelectedModel);
        }

        private ITraineeshipService TraineeshipService => this.GenericService as ITraineeshipService;
    }
}
