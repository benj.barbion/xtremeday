﻿using AutoMapper;
using BU.Extensions;
using BU.Models;
using BU.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using UI.Event;
using UI.Models;

namespace UI.ViewModels.LoggedInUser.Traineeship
{
    public enum TypeOfAction
    {
        ADDED, 
        UPDATED
    }

    public class CreateTraineeshipViewModel : BaseViewModel
    {
        readonly ITraineeshipService traineeshipService;
        readonly IMapper mapper;
        string title;

        public TypeOfAction TypeOfAction { get; private set; }
        public TraineeshipModel Traineeship { get; set; } = new(); // traineeship to add
        public ICollection<ServiceDisplayModel> ListOfServices { get; private set; }
        public ICollection<EntityModel> ListOfEntities { get; private set; }
        public ICollection<StatusModel> ListOfStatus { get; private set; } = new List<StatusModel>();
        public ICollection<LevelModel> ListOfLevels { get; private set; } = new List<LevelModel>();
        public ICollection<SportModel> ListOfSports { get; private set; }

        public string StartDate { get; set; }
        public string EndDate { get; set; }

        public CreateTraineeshipViewModel(IMapper mapper, ITraineeshipService traineeshipService, ISportService sportService, IFacilitiesService serviceService, ILocationService locationService, IEventFacade eventFacade) : base(eventFacade)
        {
            this.traineeshipService = traineeshipService;
            this.mapper = mapper;
            this.Title = "Ajouter un stage";
            this.TypeOfAction = TypeOfAction.ADDED;

            this.ListOfEntities = locationService.GetAll();
            this.ListOfSports = sportService.GetAll();
            this.ListOfServices = mapper.Map<ICollection<ServiceDisplayModel>>(serviceService.GetAll());

            Enum.GetValues(typeof(BU.TraineeshipStatus))
                .Cast<BU.TraineeshipStatus>()
                .Action(x => this.ListOfStatus.Add(x));

            Enum.GetValues(typeof(BU.TraineeshipLevel))
                .Cast<BU.TraineeshipLevel>()
                .Action(x => this.ListOfLevels.Add(x));
        }

        /// <summary>
        /// When the user want to update an existing traineeship 
        /// </summary>
        /// <param name="traineeship"></param>
        public void UpdateTraineeship(TraineeshipModel traineeship)
        {
            this.Title = "Modifier le stage";
            this.Traineeship = traineeship;
            this.TypeOfAction = TypeOfAction.UPDATED; // modify default type
            this.StartDate = traineeship.StartDate.ToString();
            this.EndDate = traineeship.EndDate.ToString();
            var idServices = this.Traineeship.Services.Select(x => x.Id); // we're taking id's from services that we have to checked
            this.ListOfServices.Where(x => idServices.Contains(x.Id)).Action(x => x.Selected = true);
            this.Refresh();
        }

        /// <summary>
        /// The user want to save Traineeship information
        /// </summary>
        public async Task Save()
        {
            // select all selected service from combobox and use automapper
            this.Traineeship.Services = mapper.Map<ICollection<ServiceModel>>(this.ListOfServices.Where(s => s.Selected)); 

            DateTime.TryParseExact(StartDate, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out var startDate);
            DateTime.TryParseExact(EndDate, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out var endDate);

            this.Traineeship.StartDate = startDate;
            this.Traineeship.EndDate = endDate;

            var action = this.TypeOfAction == TypeOfAction.ADDED ? 
                            this.traineeshipService.Create(this.Traineeship) 
                                : this.traineeshipService.Update(this.Traineeship);

            if (action.Status == BU.ServiceResultStatus.Ok)
            {
                await this.SuccessNotification();
                this.Traineeship = new();
                this.Refresh();
                await this.PublishEvent(TypeOfEvent.RefreshStatsEvent);
            }
            else
                await this.ErrorNotification(action.Message);
        }

        public StatusModel SelectedStatus
        {
            get => this.ListOfStatus.FirstOrDefault(s => s.Id == this.Traineeship.Status?.Id);
            set => this.Traineeship.Status = value;
        }

        public SportModel SelectedSport
        {
            get => this.ListOfSports.FirstOrDefault(s => s.Id == this.Traineeship.Sport?.Id);
            set => this.Traineeship.Sport = value;
        }

        public LevelModel SelectedLevel
        {
            get => this.ListOfLevels.FirstOrDefault(l => l.Id == this.Traineeship.Level?.Id);
            set => this.Traineeship.Level = value;
        }

        public EntityModel SelectedEntity
        {
            get => this.ListOfEntities.FirstOrDefault(e => e.Id == this.Traineeship.Entity?.Id);
            set => this.Traineeship.Entity = value;
        }

        public string Title
        {
            get => this.title;
            set => Set(ref this.title, value);
        }
    }
}
