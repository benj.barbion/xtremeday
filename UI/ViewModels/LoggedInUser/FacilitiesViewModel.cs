﻿using BU.Models;
using BU.Services;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UI.Event;

namespace UI.ViewModels.LoggedInUser
{
    public class FacilitiesViewModel : BaseViewModel
    {
        readonly IFacilitiesService facilitiesService;
        ServiceModel selectedFacility;

        public Collection<ServiceModel> Facilities { get; set; }
        public ServiceModel NewFacility { get; set; }
        
        public FacilitiesViewModel(IFacilitiesService service, IEventFacade eventFacade) : base(eventFacade)
        {
            this.facilitiesService = service;
            this.NewFacility = new();
        }
     
        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            this.Facilities = new ObservableCollection<ServiceModel>(this.facilitiesService.GetAll());          
            NotifyOfPropertyChange(() => this.Facilities);
        }

        public async Task Create()
        {
            if (this.NewFacility?.Name is null || this.NewFacility.Name.Length < 3)
            {
                await this.ErrorNotification("Veuillez vérifier le nom");
                return;
            }

            var service = this.facilitiesService.Create(this.NewFacility);

            if (service.Status == BU.ServiceResultStatus.Ok)
            {
                this.Facilities.Insert(0, service.ObjectResult);
                this.NewFacility = new();
                await this.SuccessNotification();
            }
            else
                await this.ErrorNotification(service.Message);
            
        }

        public bool CanUpdate => this.selectedFacility is not null;

        public async Task Update()
        {
            this.facilitiesService.Update(this.selectedFacility);
            await this.SuccessNotification();
        }

        public ServiceModel SelectedFacility
        {
            get => this.selectedFacility;
            set
            {
                Set(ref selectedFacility, value);
                this.Refresh();
            }              
        }
    }
}
