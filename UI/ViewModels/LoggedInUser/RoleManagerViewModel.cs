﻿using BU.Extensions;
using BU.Models;
using BU.Services;
using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UI.Event;
using UI.Models;

namespace UI.ViewModels.LoggedInUser
{
    public class RoleManagerViewModel : BaseViewModel
    {
        readonly IRoleService roleService;
        RoleModel selectedRole;

        public ICollection<RoleModel> ListOfRoles { get; set; }
        public ICollection<PermDisplayModel> ListOfPerms { get; set; }
        public RoleModel NewRole { get; set; }

        public RoleManagerViewModel(IRoleService roleService, IEventFacade eventAggregator) : base(eventAggregator)
        {
            this.roleService = roleService;
            this.ListOfRoles = new ObservableCollection<RoleModel>();
            this.ListOfPerms = new ObservableCollection<PermDisplayModel>();
            this.NewRole = new RoleModel();
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);

            this.ListOfRoles = new ObservableCollection<RoleModel>(this.roleService.GetAll());
            
            Enum.GetValues(typeof(BU.Perms))
                .Cast<BU.Perms>()
                .Action(x => this.ListOfPerms.Add(x));

            NotifyOfPropertyChange(() => this.ListOfRoles);
        }

        public RoleModel SelectedRole 
        {
            get => this.selectedRole; 
            set
            {
                Set(ref selectedRole, value);
                this.CheckPermsFromRole(value);
            }
        }

        private void CheckPermsFromRole(RoleModel role)
        {
            var allPerms = this.roleService.GetPerms(role);

            if (allPerms.Status == BU.ServiceResultStatus.Ok)
            {
                this.selectedRole.Perms = allPerms.ObjectResult;
                this.ListOfPerms.Apply(x => x.IsSelected = allPerms.ObjectResult.Any(ap => ap.Id == x.Id));
            }
        }

        public async Task AddRole()
        {
            if (this.NewRole?.Name is null || this.NewRole.Name.Length < 2)
            {
                await this.ErrorNotification("Veuillez vérifier le nom du rôle.");
                return;
            }

            var role = this.roleService.Create(NewRole);

            if (role.Status == BU.ServiceResultStatus.Ok)
            {
                await this.SuccessNotification();
                this.ListOfRoles.Add(role.ObjectResult);
            }
            else
                await this.ErrorNotification(role.Message);            
        }

        public async Task SavePerms()
        {
            this.ListOfPerms.Where(AddedPerms).Action(x => this.roleService.AddPerm(this.SelectedRole, x.Name));
            this.ListOfPerms.Where(DeletedPerms).Action(x => this.roleService.RemovePerm(this.SelectedRole, x.Name));
            await this.SuccessNotification();
        }

        private bool DeletedPerms(PermDisplayModel perm) => perm.IsNotSelected && this.selectedRole.Perms.Any(p => p.Id == perm.Id);

        private bool AddedPerms(PermDisplayModel perm) => perm.IsSelected && !this.selectedRole.Perms.Any(p => p.Id == perm.Id);
    }
}
