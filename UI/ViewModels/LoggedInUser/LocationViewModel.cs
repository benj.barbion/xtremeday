﻿using BU.Extensions;
using BU.Models;
using BU.Services;
using Caliburn.Micro;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using UI.Event;

namespace UI.ViewModels.LoggedInUser
{
    public class LocationViewModel : BaseViewModel
    {
        readonly ILocationService locationService;
        ProvinceModel selectedProvince;
        
        public EntityModel AddEntityToProvince { get; set; }
        public ICollection<ProvinceModel> ListOfProvinces { get; set; }
        public ICollection<EntityModel> ListOfEntities { get; set; }
        
        public LocationViewModel(ILocationService locationService, IEventFacade eventFacade) : base(eventFacade)
        {
            this.locationService = locationService;
            this.ListOfProvinces = new ObservableCollection<ProvinceModel>();           
            this.ListOfEntities = new ObservableCollection<EntityModel>();
            this.AddEntityToProvince = new EntityModel();
        }

        protected override void OnViewLoaded(object view)
        {
            base.OnViewLoaded(view);
            this.locationService.GetAllProvinces().Action(x => this.ListOfProvinces.Add(x));
        }

        public ProvinceModel SelectedProvince
        {
            get => this.selectedProvince;
            set 
            {
                this.selectedProvince = value;
                this.LoadEntities();
                this.ListOfEntities = new ObservableCollection<EntityModel>(this.selectedProvince.Entities);
                this.AddEntityToProvince.IdProvince = this.selectedProvince.Id;
                NotifyOfPropertyChange(() => this.ListOfEntities);
                NotifyOfPropertyChange(() => this.CanAddLocality);
            }
        }

        private void LoadEntities()
        {
            if (this.selectedProvince?.Entities?.Count > 0)
                return;

            this.selectedProvince.Entities = this.locationService.GetAllFrom(this.selectedProvince);
        }

        public bool CanAddLocality => this.SelectedProvince is not null;

        public async Task AddLocality()
        {
            if (this.AddEntityToProvince?.Name is null)
            {
                await this.ErrorNotification("Aucune entitée ajoutée.");
                return;
            }

            if (this.AddEntityToProvince?.PostalCode == 0)
            {
                await this.ErrorNotification("Mauvais code postal.");
                return;
            }

            if (this.ListOfEntities.Any(e => e.Name == this.AddEntityToProvince.Name))
            {
                await this.ErrorNotification("Nom d'entité déjà existant.");
                return;
            }

            this.locationService.Create(this.AddEntityToProvince);
            this.ListOfEntities.Add(this.AddEntityToProvince);
            await this.SuccessNotification();
        }
    }
}
