﻿using BU.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI.ApplicationLayer.Models;
using UI.Event;

namespace UI.ViewModels.LoggedInUser
{
    public class MyAccountViewModel : BaseViewModel
    {
        readonly LoggedInUserContext loggedInUser;
        readonly IUserService userService;
        string oldPassword;
        string newPassword;
        string repeatNewPassword;

        public MyAccountViewModel(IEventFacade eventFacade, LoggedInUserContext loggedInUser, IUserService userService) : base(eventFacade)
        {
            this.loggedInUser = loggedInUser;
            this.userService = userService;
        }

        public string OldPassword
        {
            get => oldPassword;
            set { oldPassword = value; NotifyOfPropertyChange(() => CanModifyPassword); }
        }

        public string NewPassword
        {
            get => newPassword;
            set { newPassword = value; NotifyOfPropertyChange(() => CanModifyPassword); }
        }

        public string RepeatNewPassword
        {
            get => repeatNewPassword;
            set { repeatNewPassword = value; NotifyOfPropertyChange(() => CanModifyPassword); }
        }

        public bool CanModifyPassword => OldPassword is not null && NewPassword?.Length > 3 && NewPassword == RepeatNewPassword;

        public async Task ModifyPassword()
        {
            var result = this.userService.ChangePassword(this.loggedInUser, oldPassword, newPassword);

            if (result.Status == BU.ServiceResultStatus.Ok)
                await this.SuccessNotification();
            else
                await this.ErrorNotification(result.Message);
        }
    }
}
