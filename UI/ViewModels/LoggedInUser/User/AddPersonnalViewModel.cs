﻿using AutoMapper;
using BU.Models;
using BU.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UI.Event;
using UI.Models;
using UI.ViewModels.LoggedInUser.User;

namespace UI.ViewModels.LoggedInUser
{
    public class AddPersonnalViewModel : AddPersonViewModel<UserModel>
    {
        readonly IMapper mapper;
        
        public ICollection<RoleDisplayModel> Roles { get; set; }
       
        public AddPersonnalViewModel(IUserService userService, ILocationService locationService, IRoleService roleService, IMapper mapper, IEventFacade eventFacade) : base(userService, locationService, eventFacade)
        {
            this.mapper = mapper;
            this.Roles = mapper.Map<ICollection<RoleDisplayModel>>(roleService.GetAll());
        }
       
        public override void Save()
        {
            this.Model.UserRoles = mapper.Map<ICollection<RoleModel>>(this.Roles.Where(s => s.Selected));          
            this.Model.Person.Birtdhday = DateTime.ParseExact(BirthdayDate, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

            var user = this.GenericService.Create(this.Model);

            if (user.Status == BU.ServiceResultStatus.Ok)
                TryCloseAsync();
            else
                this.Message = $"Erreur : {user.Message}";
        }
    }
}
