﻿using BU.Models;
using BU.Services;
using System;
using System.Globalization;
using UI.Event;
using UI.ViewModels.LoggedInUser.User;

namespace UI.ViewModels.LoggedInUser.Traineeship.Registration
{
    public class AddParticipantViewModel : AddPersonViewModel<ClientModel>
    {
        public AddParticipantViewModel(IClientService clientService, ILocationService locationService, IEventFacade eventFacade) : base(clientService, locationService, eventFacade)
        {
        }

        public override void Save()
        {
            this.Model.Person.Birtdhday = DateTime.ParseExact(BirthdayDate, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None);

            var client = this.GenericService.Create(this.Model);
            
            if (client.Status == BU.ServiceResultStatus.Ok)
            {
                this.Model = client.ObjectResult;
                _ = this.PublishEvent(TypeOfEvent.RefreshStatsEvent);
                TryCloseAsync();
            }                  
            else
                this.Message = $"Erreur : {client.Message}";
        }
    }
}
