﻿using BU.Models;
using BU.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI.Event;

namespace UI.ViewModels.LoggedInUser.User
{
    public abstract class AddPersonViewModel<TModel> : BaseViewModel where TModel : class , new()
    {
        string message;
        string birthDayDate;

        public IGenericService<TModel> GenericService { get; private set; }
        public ICollection<EntityModel> ListOfEntities { get; set; }
        public TModel Model { get; set; }

        public AddPersonViewModel(IGenericService<TModel> service, ILocationService locationService, IEventFacade eventFacade) : base(eventFacade)
        {
            this.GenericService = service;
            this.ListOfEntities = locationService.GetAll();
            this.Model = new();
            this.Message = "Veuillez remplir les champs.";
        }

        public string Message
        {
            get => message;
            set => Set(ref message, value);
        }

        public bool CanSave => DateTime.TryParseExact(BirthdayDate, "dd-MM-yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out var birthday);

        public virtual void Save()
        {
            var user = this.GenericService.Create(this.Model);

            if (user.Status == BU.ServiceResultStatus.Ok)
                this.Message = "Opération réussie !";
            else
                this.Message = $"Erreur : {user.Message}";
        }

        public string BirthdayDate
        {
            get => this.birthDayDate;
            set
            {
                this.birthDayDate = value;
                NotifyOfPropertyChange(() => this.CanSave);
            }
        }
    }
}
