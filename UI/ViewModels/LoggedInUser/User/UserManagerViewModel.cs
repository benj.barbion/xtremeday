﻿using Caliburn.Micro;
using BU.Models;
using BU.Services;
using UI.Helper;
using UI.ApplicationLayer.Models;
using UI.Event;
using System.Threading.Tasks;
using BU.Extensions;

namespace UI.ViewModels.LoggedInUser
{
    public class UserManagerViewModel : ListBaseViewModel<UserModel>
    {
        readonly LoggedInUserContext loggedInUserContext;
        readonly IModalHelper modalHelper;

        public string Message { get; private set; }

        public UserManagerViewModel(IUserService userService, IModalHelper modalHelper, LoggedInUserContext loggedInUserContext, IEventFacade eventFacade) : base(userService, eventFacade)
        {
            this.modalHelper = modalHelper;
            this.loggedInUserContext = loggedInUserContext;
        }

        public async Task AddUser()
        {
            var user = IoC.Get<AddPersonnalViewModel>();
            await this.modalHelper.CreateModal(user, "Ajouter un membre du personnel");
            if (user.Model?.Password is not null)
            {
                this.Models.Insert(0, user.Model);
                this.Message = $"Enregistrez le mot de passe : {user.Model.Password}";
                NotifyOfPropertyChange(() => this.Message);
            }
        }

        public async Task DeleteUser()
        {
            if (this.SelectedModel?.Id == loggedInUserContext?.Id)
            {
                await this.ErrorNotification("Vous ne pouvez pas vous supprimer !");
                return;
            }

            this.GenericService.Delete(this.SelectedModel.Id);
            this.Models.Remove(this.SelectedModel);
            await this.SuccessNotification();
        }

        public override bool FilterCollection(object obj)
        {
            var user = obj as UserModel;
            
            if (this.Filter is null || user is null)
                return true;

            if (user.Person?.LastName?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (user.Person?.FirstName?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (user.Person?.Email?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (user.Person?.Entity?.Name?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            return false;
        }
    }
}
