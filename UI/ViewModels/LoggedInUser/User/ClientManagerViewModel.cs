﻿using BU.Extensions;
using BU.Models;
using BU.Services;
using Caliburn.Micro;
using System.Threading.Tasks;
using UI.Event;
using UI.Helper;
using UI.ViewModels.LoggedInUser.Traineeship.Registration;

namespace UI.ViewModels.LoggedInUser
{
    public class ClientManagerViewModel : ListBaseViewModel<ClientModel>
    {
        readonly IModalHelper modalHelper;

        public ClientManagerViewModel(IClientService clientService, IEventFacade eventFacade, IModalHelper modalHelper) : base(clientService, eventFacade)
        {
            this.modalHelper = modalHelper;
        }

        public async Task AddUser()
        {
            var client = IoC.Get<AddParticipantViewModel>();
            await this.modalHelper.CreateModal(client, "Ajouter un stagiaire");
            if (client.Model.Person.Email is not null)
                this.Models.Insert(0, client.Model);
        }

        public override bool FilterCollection(object obj)
        {
            var user = obj as ClientModel;

            if (this.Filter is null || user is null)
                return true;

            if (user.Person?.LastName?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (user.Person?.FirstName?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (user.Person?.Email?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            if (user.Person?.Entity?.Name?.ContainsIgnoreCase(this.Filter) ?? false)
                return true;

            return false;
        }
    }
}
