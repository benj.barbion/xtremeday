﻿using BU.Models;
using System;
using System.Collections.Generic;

namespace UI.ApplicationLayer.Models
{
    public class LoggedInUserContext : UserModel
    {
        public void Reset()
        {
            this.Id = 0;
            this.UserRoles = null;
            this.Person = null;
        }

        internal void Create(UserModel loggedInUserContext)
        {
            if (loggedInUserContext == null)
                throw new ArgumentNullException(nameof(loggedInUserContext));

            this.Id = loggedInUserContext.Id;
            this.UserRoles = loggedInUserContext.UserRoles; 
            this.Person = loggedInUserContext.Person;
        }
    }
}
