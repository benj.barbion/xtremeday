﻿namespace UI.ApplicationLayer.Services
{
    public interface IPermApplicationService
    {
        bool HasPerm(BU.Perms perm);
        bool IsInRole(string role);
    }
}