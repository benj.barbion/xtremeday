﻿using BU.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI.ApplicationLayer.Models;

namespace UI.ApplicationLayer.Services
{
    public class PermApplicationService : IPermApplicationService
    {
        private readonly LoggedInUserContext loggedInUser;

        public PermApplicationService(LoggedInUserContext loggedInUser)
        {
            this.loggedInUser = loggedInUser;
        }

        public bool HasPerm(BU.Perms perm)
        {
            return this.loggedInUser?.UserRoles?.Any(x => x.Perms?.Any(p => p.Name == perm.ToString()) ?? false) ?? false;
        }

        public bool IsInRole(string role)
        {
            return this.loggedInUser.UserRoles.Any(x => x.Name == role);
        }
    }
}