﻿using System;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DAL
{
    public partial class XtremeDayContext : DbContext
    {
        public XtremeDayContext(DbContextOptions<XtremeDayContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Entity> Entities { get; set; }
        public virtual DbSet<Level> Levels { get; set; }
        public virtual DbSet<Perm> Perms { get; set; }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<Province> Provinces { get; set; }
        public virtual DbSet<Registration> Registrations { get; set; }
        public virtual DbSet<RegistrationClient> RegistrationClients { get; set; }
        public virtual DbSet<RegistrationStatus> RegistrationStatuses { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RolePerm> RolePerms { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<Sport> Sports { get; set; }
        public virtual DbSet<Traineeship> Traineeships { get; set; }
        public virtual DbSet<TraineeshipService> TraineeshipServices { get; set; }
        public virtual DbSet<TraineeshipStatus> TraineeshipStatuses { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "French_CI_AS");

            modelBuilder.Entity<Client>(entity =>
            {
                entity.ToTable("Client");

                entity.HasOne(d => d.IdPersonNavigation)
                    .WithMany(p => p.Clients)
                    .HasForeignKey(d => d.IdPerson)
                    .HasConstraintName("FK__Client__IdPerson__73852659");
            });

            modelBuilder.Entity<Entity>(entity =>
            {
                entity.ToTable("Entity");

                entity.HasIndex(e => new { e.PostalCode, e.Name, e.IdProvince }, "UQ__Entity__B7E95FAC2B355C85")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.IdProvinceNavigation)
                    .WithMany(p => p.Entities)
                    .HasForeignKey(d => d.IdProvince)
                    .HasConstraintName("FK__Entity__IdProvin__4E53A1AA");
            });

            modelBuilder.Entity<Level>(entity =>
            {
                entity.ToTable("Level");

                entity.HasIndex(e => e.Name, "NameIndex");

                entity.HasIndex(e => e.Name, "UQ__Level__737584F6400A7641")
                    .IsUnique();

                entity.Property(e => e.Desc).HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Perm>(entity =>
            {
                entity.ToTable("Perm");

                entity.HasIndex(e => e.Name, "UQ__Perm__737584F606D16960")
                    .IsUnique();

                entity.Property(e => e.Desc).HasMaxLength(300);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.ToTable("Person");

                entity.HasIndex(e => e.Email, "UQ__Person__A9D1053414308F5D")
                    .IsUnique();

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(500);

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.PhoneNumber)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.IdEntityNavigation)
                    .WithMany(p => p.People)
                    .HasForeignKey(d => d.IdEntity)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Person__IdEntity__634EBE90");
            });

            modelBuilder.Entity<Province>(entity =>
            {
                entity.ToTable("Province");

                entity.HasIndex(e => e.Name, "NameIndex");

                entity.HasIndex(e => e.Name, "UQ__Province__737584F68DC70052")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Registration>(entity =>
            {
                entity.ToTable("Registration");

                entity.Property(e => e.Code).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Date)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.IdRegistrationStatusNavigation)
                    .WithMany(p => p.Registrations)
                    .HasForeignKey(d => d.IdRegistrationStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Registrat__IdReg__0C50D423");

                entity.HasOne(d => d.IdResponsibleClientNavigation)
                    .WithMany(p => p.Registrations)
                    .HasForeignKey(d => d.IdResponsibleClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Registrat__IdRes__0B5CAFEA");

                entity.HasOne(d => d.IdTraineeshipNavigation)
                    .WithMany(p => p.Registrations)
                    .HasForeignKey(d => d.IdTraineeship)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Registrat__IdTra__0A688BB1");
            });

            modelBuilder.Entity<RegistrationClient>(entity =>
            {
                entity.HasKey(e => new { e.IdRegistration, e.IdClient })
                    .HasName("PK__Registra__8E65DDEAB2940357");

                entity.ToTable("RegistrationClient");

                entity.HasOne(d => d.IdClientNavigation)
                    .WithMany(p => p.RegistrationClients)
                    .HasForeignKey(d => d.IdClient)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Registrat__IdCli__10216507");

                entity.HasOne(d => d.IdRegistrationNavigation)
                    .WithMany(p => p.RegistrationClients)
                    .HasForeignKey(d => d.IdRegistration)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Registrat__IdReg__0F2D40CE");
            });

            modelBuilder.Entity<RegistrationStatus>(entity =>
            {
                entity.ToTable("RegistrationStatus");

                entity.HasIndex(e => e.Name, "NameIndex");

                entity.HasIndex(e => e.Name, "UQ__Registra__737584F60E088786")
                    .IsUnique();

                entity.Property(e => e.Desc).HasMaxLength(200);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("Role");

                entity.HasIndex(e => e.Name, "UQ__Role__737584F67B90EE8A")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<RolePerm>(entity =>
            {
                entity.HasKey(e => new { e.IdRole, e.IdPerm })
                    .HasName("PK__RolePerm__5BEEB31A5DE95254");

                entity.HasOne(d => d.IdPermNavigation)
                    .WithMany(p => p.RolePerms)
                    .HasForeignKey(d => d.IdPerm)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RolePerms__IdPer__04AFB25B");

                entity.HasOne(d => d.IdRoleNavigation)
                    .WithMany(p => p.RolePerms)
                    .HasForeignKey(d => d.IdRole)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__RolePerms__IdRol__03BB8E22");
            });

            modelBuilder.Entity<Service>(entity =>
            {
                entity.ToTable("Service");

                entity.HasIndex(e => e.Name, "NameIndex");

                entity.HasIndex(e => e.Name, "UQ__Service__737584F6CA105CAD")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Sport>(entity =>
            {
                entity.ToTable("Sport");

                entity.HasIndex(e => e.Name, "NameIndex");

                entity.HasIndex(e => e.Name, "UQ__Sport__737584F6CBFC1AEA")
                    .IsUnique();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<Traineeship>(entity =>
            {
                entity.ToTable("Traineeship");

                entity.Property(e => e.Address).IsRequired();

                entity.Property(e => e.Code).HasDefaultValueSql("(newid())");

                entity.Property(e => e.CreateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.PricePerPerson).HasColumnType("smallmoney");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.HasOne(d => d.IdEntityNavigation)
                    .WithMany(p => p.Traineeships)
                    .HasForeignKey(d => d.IdEntity)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Traineesh__IdEnt__57DD0BE4");

                entity.HasOne(d => d.IdLevelNavigation)
                    .WithMany(p => p.Traineeships)
                    .HasForeignKey(d => d.IdLevel)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Traineesh__IdLev__55F4C372");

                entity.HasOne(d => d.IdSportNavigation)
                    .WithMany(p => p.Traineeships)
                    .HasForeignKey(d => d.IdSport)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Traineesh__IdSpo__56E8E7AB");

                entity.HasOne(d => d.IdTraineeshipStatusNavigation)
                    .WithMany(p => p.Traineeships)
                    .HasForeignKey(d => d.IdTraineeshipStatus)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Traineesh__IdTra__58D1301D");
            });

            modelBuilder.Entity<TraineeshipService>(entity =>
            {
                entity.HasKey(e => new { e.IdTraineeship, e.IdService })
                    .HasName("PK__Trainees__7F7ECACCFD5CDE3B");

                entity.ToTable("TraineeshipService");

                entity.HasOne(d => d.IdServiceNavigation)
                    .WithMany(p => p.TraineeshipServices)
                    .HasForeignKey(d => d.IdService)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Traineesh__IdSer__5F7E2DAC");

                entity.HasOne(d => d.IdTraineeshipNavigation)
                    .WithMany(p => p.TraineeshipServices)
                    .HasForeignKey(d => d.IdTraineeship)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Traineesh__IdTra__5E8A0973");
            });

            modelBuilder.Entity<TraineeshipStatus>(entity =>
            {
                entity.ToTable("TraineeshipStatus");

                entity.HasIndex(e => e.Name, "NameIndex");

                entity.HasIndex(e => e.Name, "UQ__Trainees__737584F698CC9A2F")
                    .IsUnique();

                entity.Property(e => e.Desc).HasMaxLength(300);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200);
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");

                entity.Property(e => e.Active)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Password).IsRequired();

                entity.HasOne(d => d.IdPersonNavigation)
                    .WithMany(p => p.Users)
                    .HasForeignKey(d => d.IdPerson)
                    .HasConstraintName("FK__User__IdPerson__7755B73D");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.HasKey(e => new { e.IdUser, e.IdRole })
                    .HasName("PK__UserRole__EC8A4F3D899942CB");

                entity.HasOne(d => d.IdRoleNavigation)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.IdRole)
                    .HasConstraintName("FK__UserRoles__IdRol__7E02B4CC");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.IdUser)
                    .HasConstraintName("FK__UserRoles__IdUse__7D0E9093");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
