﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    //Only for sample
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        protected readonly DbContext context;

        public GenericRepository(DbContext context)
        {
            this.context = context;
        }

        public T Create(T model)
        {
            this.context.Add<T>(model);
            this.context.SaveChanges();
            return model;
        }

        public int Delete(T model)
        {
            this.context.Set<T>().Remove(model);
            return this.context.SaveChanges();
        }

        public ICollection<T> Find(Expression<Func<T, bool>> predicate)
        {
            return this.context.Set<T>().Where(predicate).ToList();
        }

        public T FindById(int id)
        {
            return this.context.Set<T>().Find(id);
        }

        public ICollection<T> GetAll()
        {
            return this.context.Set<T>().ToList();
        }

        public int Update(T model)
        {
            this.context.Set<T>().Update(model);
            return this.context.SaveChanges();
        }
    }
}
