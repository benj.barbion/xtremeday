﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    //Only for sample
    //Update - Set - Change - FindBy - Add
    //https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.identity.usermanager-1.setusernameasync?view=aspnetcore-6.0
    public interface IGenericRepository<T> where T : class
    {
        ICollection<T> GetAll();
        T Create(T model);
        int Update(T model);
        int Delete(T model);
        T FindById(int id);
        ICollection<T> Find(Expression<Func<T, bool>> predicate);
    }
}
