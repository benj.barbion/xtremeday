﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Traineeship
    {
        public Traineeship()
        {
            Registrations = new HashSet<Registration>();
            TraineeshipServices = new HashSet<TraineeshipService>();
        }

        public int Id { get; set; }
        public Guid? Code { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public short MaxInscription { get; set; }
        public short RemainingInscription { get; set; }
        public decimal PricePerPerson { get; set; }
        public string Address { get; set; }
        public int IdLevel { get; set; }
        public int IdSport { get; set; }
        public int IdEntity { get; set; }
        public int IdTraineeshipStatus { get; set; }

        public virtual Entity IdEntityNavigation { get; set; }
        public virtual Level IdLevelNavigation { get; set; }
        public virtual Sport IdSportNavigation { get; set; }
        public virtual TraineeshipStatus IdTraineeshipStatusNavigation { get; set; }

        public object Include()
        {
            throw new NotImplementedException();
        }

        public virtual ICollection<Registration> Registrations { get; set; }
        public virtual ICollection<TraineeshipService> TraineeshipServices { get; set; }
    }
}
