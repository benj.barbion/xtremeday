﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Perm
    {
        public Perm()
        {
            RolePerms = new HashSet<RolePerm>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }

        public virtual ICollection<RolePerm> RolePerms { get; set; }
    }
}
