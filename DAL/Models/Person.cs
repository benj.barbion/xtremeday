﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Person
    {
        public Person()
        {
            Clients = new HashSet<Client>();
            Users = new HashSet<User>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public DateTime Birthday { get; set; }
        public int IdEntity { get; set; }

        public virtual Entity IdEntityNavigation { get; set; }
        public virtual ICollection<Client> Clients { get; set; }
        public virtual ICollection<User> Users { get; set; }
    }
}
