﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Entity
    {
        public Entity()
        {
            People = new HashSet<Person>();
            Traineeships = new HashSet<Traineeship>();
        }

        public int Id { get; set; }
        public int PostalCode { get; set; }
        public string Name { get; set; }
        public int IdProvince { get; set; }

        public virtual Province IdProvinceNavigation { get; set; }
        public virtual ICollection<Person> People { get; set; }
        public virtual ICollection<Traineeship> Traineeships { get; set; }
    }
}
