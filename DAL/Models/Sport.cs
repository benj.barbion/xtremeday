﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Sport
    {
        public Sport()
        {
            Traineeships = new HashSet<Traineeship>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Traineeship> Traineeships { get; set; }
    }
}
