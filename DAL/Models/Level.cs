﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Level
    {
        public Level()
        {
            Traineeships = new HashSet<Traineeship>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }

        public virtual ICollection<Traineeship> Traineeships { get; set; }
    }
}
