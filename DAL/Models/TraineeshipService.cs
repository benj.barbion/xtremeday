﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class TraineeshipService
    {
        public int IdTraineeship { get; set; }
        public int IdService { get; set; }

        public virtual Service IdServiceNavigation { get; set; }
        public virtual Traineeship IdTraineeshipNavigation { get; set; }
    }
}
