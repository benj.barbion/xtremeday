﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class RolePerm
    {
        public int IdRole { get; set; }
        public int IdPerm { get; set; }

        public virtual Perm IdPermNavigation { get; set; }
        public virtual Role IdRoleNavigation { get; set; }
    }
}
