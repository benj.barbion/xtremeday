﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class RegistrationClient
    {
        public int IdRegistration { get; set; }
        public int IdClient { get; set; }

        public virtual Client IdClientNavigation { get; set; }
        public virtual Registration IdRegistrationNavigation { get; set; }
    }
}
