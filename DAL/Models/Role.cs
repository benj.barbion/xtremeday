﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Role
    {
        public Role()
        {
            RolePerms = new HashSet<RolePerm>();
            UserRoles = new HashSet<UserRole>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<RolePerm> RolePerms { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
