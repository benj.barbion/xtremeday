﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Client
    {
        public Client()
        {
            RegistrationClients = new HashSet<RegistrationClient>();
            Registrations = new HashSet<Registration>();
        }

        public int Id { get; set; }
        public int IdPerson { get; set; }

        public virtual Person IdPersonNavigation { get; set; }
        public virtual ICollection<RegistrationClient> RegistrationClients { get; set; }
        public virtual ICollection<Registration> Registrations { get; set; }
    }
}
