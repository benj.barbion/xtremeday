﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Province
    {
        public Province()
        {
            Entities = new HashSet<Entity>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Entity> Entities { get; set; }
    }
}
