﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Service
    {
        public Service()
        {
            TraineeshipServices = new HashSet<TraineeshipService>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }

        public virtual ICollection<TraineeshipService> TraineeshipServices { get; set; }
    }
}
