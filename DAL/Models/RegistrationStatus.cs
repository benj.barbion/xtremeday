﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class RegistrationStatus
    {
        public RegistrationStatus()
        {
            Registrations = new HashSet<Registration>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }

        public virtual ICollection<Registration> Registrations { get; set; }
    }
}
