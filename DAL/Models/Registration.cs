﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DAL.Models
{
    public partial class Registration
    {
        public Registration()
        {
            RegistrationClients = new HashSet<RegistrationClient>();
        }

        public int Id { get; set; }
        public Guid? Code { get; set; }
        public DateTime Date { get; set; }
        public int IdTraineeship { get; set; }
        public int IdResponsibleClient { get; set; }
        public int IdRegistrationStatus { get; set; }

        public virtual RegistrationStatus IdRegistrationStatusNavigation { get; set; }
        public virtual Client IdResponsibleClientNavigation { get; set; }
        public virtual Traineeship IdTraineeshipNavigation { get; set; }
        public virtual ICollection<RegistrationClient> RegistrationClients { get; set; }
    }
}
